﻿using System;
using System.Collections.Generic;
using ResumeBuilder.Domain.DomainModel;
using System.ComponentModel.DataAnnotations;


namespace ResumeBuilder.ViewModels
{
    public class EmailTemplatesViewModel
    {
        public IList<EmailTemplates> emailtemplates { get; set; }
    }

    public class EmailTemplatesDetails
    {
        public EmailTemplates emailtemplates { get; set; }
    }

    public class EmailTemplatesAddModel
    {
        [Required]
        [StringLength(150)]
        [Display(Name = "Template Name")]
        public string TemplateName { get; set; }

        [Required]
        [StringLength(255)]
        [Display(Name = "Template Subject")]
        public string TemplateSubject { get; set; }

        [Required]
        [StringLength(1500)]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Template Body")]
        public string TemplateBody { get; set; }

        [Display(Name = "Status")]
        public bool IsActive { get; set; }

    }

    public class EmailTemplatesEditModel
    {
        [Required]
        public Guid Id { get; set; }

        [Required]
        [Display(Name = "Template ID")]
        public string TemplateId { get; set; }

        [Required]
        [StringLength(150)]
        [Display(Name = "Template Name")]
        public string TemplateName { get; set; }

        [Required]
        [StringLength(150)]
        [Display(Name = "Template Subject")]
        public string TemplateSubject { get; set; }

        [Required]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Template Body")]
        public string TemplateBody { get; set; }

        public DateTime? LastModified { get; set; }
        [Display(Name = "Status")]
        public bool IsActive { get; set; }

    }

}
