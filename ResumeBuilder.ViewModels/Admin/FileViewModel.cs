﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ResumeBuilder.ViewModels.Admin
{
    public class FileViewModel
    {
        public string Folder { get; set; }
        public string SubFolder { get; set; }
        public string ConnectorUrl { get; set; }
        public string Type { get; set; }
    }
}
