﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.ViewModels.Admin
{
    public class EditSettingsViewModel
    {
        public Guid Id { get; set; }

        [DisplayName("Forum Name")]
        [Required]
        [StringLength(200)]
        public string ApplicationName { get; set; }

        [DisplayName("Forum Url")]
        [Required]
        [StringLength(200)]
        [Url]
        public string ApplicationUrl { get; set; }

        [DisplayName("Close Forum")]
        [Description("Close the forum for maintenance")]
        public bool IsClosed { get; set; }

        [DisplayName("Allow Rss Feeds")]
        [Description("Show the RSS feed icons for the Topics and Categories")]
        public bool EnableRSSFeeds { get; set; }

        [DisplayName("Show Edited By Details On Posts")]
        public bool DisplayEditedBy { get; set; }

        //[DisplayName("Allow File Attachments On Posts")]
        //public bool EnablePostFileAttachments { get; set; }

        [DisplayName("Allow Posts To Be Marked As Solution")]
        public bool EnableMarkAsSolution { get; set; }

        [DisplayName("Enable Spam Reporting")]
        public bool EnableSpamReporting { get; set; }

        [DisplayName("Allow Members To Report Other Members")]
        public bool EnableMemberReporting { get; set; }

        [DisplayName("Allow Email Subscriptions")]
        public bool EnableEmailSubscriptions { get; set; }

        [DisplayName("Manually Authorize New Members")]
        public bool ManuallyAuthoriseNewMembers { get; set; }

        [DisplayName("Email Admin On New Member Signup")]
        public bool EmailAdminOnNewMemberSignUp { get; set; }

        [DisplayName("Allow Member Signatures")]
        public bool EnableSignatures { get; set; }

        [EmailAddress]
        [DisplayName("Admin Email Address")]
        public string AdminEmailAddress { get; set; }

        [EmailAddress]
        [DisplayName("Notification Reply Email Address")]
        public string NotificationReplyEmail { get; set; }

        [DisplayName("SMTP Server")]
        public string SMTP { get; set; }

        [DisplayName("SMTP Server Username")]
        public string SMTPUsername { get; set; }

        [DisplayName("SMTP Server Password")]
        public string SMTPPassword { get; set; }

        [DisplayName("Optional: SMTP Port")]
        public int? SMTPPort { get; set; }

        [DisplayName("SMTP SSL - Enable SSL for sending via gmail etc...")]
        public bool SMTPEnableSSL { get; set; }

        public List<string> Themes { get; set; }

        [DisplayName("New Member Starting Role")]
        public Guid? NewMemberStartingRole { get; set; }

        public List<MembershipRole> Roles { get; set; }

        [DisplayName("Enable Akismet Spam Control")]
        public bool EnableAkisment { get; set; }

        [DisplayName("Enter Your Akismet Key Here")]
        public string AkismentKey { get; set; }

        [DisplayName("Enter a Spam registration prevention question")]
        public string SpamQuestion { get; set; }

        [DisplayName("Enter the answer to your Spam question")]
        public string SpamAnswer { get; set; }

        [DisplayName("Suspend the registration (Don't allow any new members to register)")]
        public bool SuspendRegistration { get; set; }

    }
}