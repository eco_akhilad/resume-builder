﻿using System.Collections.Generic;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.ViewModels.Admin
{
    public class ListLogViewModel
    {
        public IList<LogEntry> LogFiles { get; set; }
    }
}