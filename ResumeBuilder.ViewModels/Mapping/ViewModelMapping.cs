﻿using System;
using System.Linq;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.ViewModels.Admin;

namespace ResumeBuilder.ViewModels.Mapping
{
    public static class ViewModelMapping
    {
        public static ResumeBuilder.ViewModels.Admin.SingleMemberListViewModel UserToSingleMemberListViewModel(MembershipUser user)
        {
            var viewModel = new ResumeBuilder.ViewModels.Admin.SingleMemberListViewModel
                            {
                                IsApproved = user.IsApproved,
                                Id = user.Id,
                                IsLockedOut = user.IsLockedOut,
                                Roles = user.Roles.Select(x => x.RoleName).ToArray(),
                                UserName = user.UserName
                            };
            return viewModel;
        }

        public static ResumeBuilder.ViewModels.Admin.MemberEditViewModel UserToMemberEditViewModel(MembershipUser user)
        {
            var viewModel = new ResumeBuilder.ViewModels.Admin.MemberEditViewModel
            {
                Id = user.Id,
                IsApproved = user.IsApproved,
                IsDeleted = user.IsDeleted,
                IsLockedOut = user.IsLockedOut,
                Roles = user.Roles.Select(x => x.RoleName).ToArray(),
                UserName = user.UserName,
                Comment = user.Comment,
                CreateDate = user.CreateDate,
                Email = user.Email,
                FailedPasswordAnswerAttempt = user.FailedPasswordAnswerAttempt,
                FailedPasswordAttemptCount = user.FailedPasswordAttemptCount,
                LastLockoutDate = user.LastLockoutDate,
                LastLoginDate = user.LastLoginDate,
                LastPasswordChangedDate = user.LastPasswordChangedDate,
                Location = user.Location,
                PasswordAnswer = user.PasswordAnswer,
                PasswordQuestion = user.PasswordQuestion,
                Signature = user.Signature,
                Website = user.Website,
            };
            return viewModel;
        }

        public static RoleViewModel RoleToRoleViewModel(MembershipRole role)
        {
            var viewModel = new RoleViewModel
            {
                Id = role.Id,
                RoleName = role.RoleName
            };
            return viewModel;
        }

        public static MembershipRole RoleViewModelToRole(ResumeBuilder.ViewModels.Admin.RoleViewModel roleViewModel)
        {
            var viewModel = new MembershipRole
            {
                RoleName = roleViewModel.RoleName
            };
            return viewModel;
        }

        public static Settings SettingsViewModelToSettings(ResumeBuilder.ViewModels.Admin.EditSettingsViewModel settingsViewModel, Settings existingSettings)
        {
            existingSettings.Id = settingsViewModel.Id;
            existingSettings.ApplicationUrl = settingsViewModel.ApplicationUrl;
            existingSettings.ApplicationName = settingsViewModel.ApplicationName;
            existingSettings.IsClosed = settingsViewModel.IsClosed;
            existingSettings.EnableRSSFeeds = settingsViewModel.EnableRSSFeeds;
            existingSettings.DisplayEditedBy = settingsViewModel.DisplayEditedBy;
            existingSettings.EnableMarkAsSolution = settingsViewModel.EnableMarkAsSolution;
            existingSettings.EnableSpamReporting = settingsViewModel.EnableSpamReporting;
            existingSettings.EnableMemberReporting = settingsViewModel.EnableMemberReporting;
            existingSettings.ManuallyAuthoriseNewMembers = settingsViewModel.ManuallyAuthoriseNewMembers;
            existingSettings.EmailAdminOnNewMemberSignUp = settingsViewModel.EmailAdminOnNewMemberSignUp;
            existingSettings.EnableSignatures = settingsViewModel.EnableSignatures;
            existingSettings.AdminEmailAddress = settingsViewModel.AdminEmailAddress;
            existingSettings.NotificationReplyEmail = settingsViewModel.NotificationReplyEmail;
            existingSettings.SMTP = settingsViewModel.SMTP;
            existingSettings.SMTPUsername = settingsViewModel.SMTPUsername;
            existingSettings.SMTPPassword = settingsViewModel.SMTPPassword;
            existingSettings.AkismentKey = settingsViewModel.AkismentKey;
            existingSettings.EnableAkisment = settingsViewModel.EnableAkisment;
            existingSettings.SMTPPort = settingsViewModel.SMTPPort.ToString();
            existingSettings.SpamQuestion = settingsViewModel.SpamQuestion;
            existingSettings.SpamAnswer = settingsViewModel.SpamAnswer;
            existingSettings.SMTPEnableSSL = settingsViewModel.SMTPEnableSSL;
            existingSettings.SuspendRegistration = settingsViewModel.SuspendRegistration;

            return existingSettings;
        }

        public static ResumeBuilder.ViewModels.Admin.EditSettingsViewModel SettingsToSettingsViewModel(Settings currentSettings)
        {
            var settingViewModel = new ResumeBuilder.ViewModels.Admin.EditSettingsViewModel
            {
                Id = currentSettings.Id,
                ApplicationName = currentSettings.ApplicationName,
                ApplicationUrl = currentSettings.ApplicationUrl,
                IsClosed = currentSettings.IsClosed,
                EnableRSSFeeds = currentSettings.EnableRSSFeeds,
                DisplayEditedBy = currentSettings.DisplayEditedBy,
                EnableMarkAsSolution = currentSettings.EnableMarkAsSolution,
                EnableSpamReporting = currentSettings.EnableSpamReporting,
                EnableMemberReporting = currentSettings.EnableMemberReporting,
                ManuallyAuthoriseNewMembers = currentSettings.ManuallyAuthoriseNewMembers,
                EmailAdminOnNewMemberSignUp = currentSettings.EmailAdminOnNewMemberSignUp,
                EnableSignatures = currentSettings.EnableSignatures,
                AdminEmailAddress = currentSettings.AdminEmailAddress,
                NotificationReplyEmail = currentSettings.NotificationReplyEmail,
                SMTP = currentSettings.SMTP,
                SMTPUsername = currentSettings.SMTPUsername,
                SMTPPassword = currentSettings.SMTPPassword,
                AkismentKey = currentSettings.AkismentKey,
                EnableAkisment = currentSettings.EnableAkisment != null && (bool)currentSettings.EnableAkisment,
                SMTPPort = string.IsNullOrEmpty(currentSettings.SMTPPort) ? null : (int?)(Convert.ToInt32(currentSettings.SMTPPort)),
                SpamQuestion = currentSettings.SpamQuestion,
                SpamAnswer = currentSettings.SpamAnswer,
                SMTPEnableSSL = currentSettings.SMTPEnableSSL ?? false,
                SuspendRegistration = currentSettings.SuspendRegistration ?? false,
            };

            return settingViewModel;
        }
    }
}