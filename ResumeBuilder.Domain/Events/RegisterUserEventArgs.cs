﻿using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Events
{
    public class RegisterUserEventArgs : ApplicationEventArgs
    {
        public MembershipUser User { get; set; }
        public MembershipCreateStatus CreateStatus { get; set; }
    }
}
