﻿using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Events
{
    public class UpdateProfileEventArgs : ApplicationEventArgs
    {
        public MembershipUser User { get; set; }
    }
}
