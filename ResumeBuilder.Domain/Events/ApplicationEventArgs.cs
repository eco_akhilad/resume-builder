﻿using System;
using ResumeBuilder.Domain.Interfaces.API;

namespace ResumeBuilder.Domain.Events
{
    public abstract class ApplicationEventArgs : EventArgs
    {
        public IApplicationAPI Api { get; set; }
        public bool Cancel { get; set; }
    }
}
