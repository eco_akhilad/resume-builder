﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ResumeBuilder.Utilities;

namespace ResumeBuilder.Domain.DomainModel
{
    public class History : Entity
    {
        public History()
        {
            Id = 0;
            Timestamp = System.DateTime.Now;
        }

        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string Data { get; set; }
        public string TableName { get; set; }
        public string Type { get; set; }
        public Guid Item_Id { get; set; }

        public virtual MembershipUser User { get; set; }
    }
}
