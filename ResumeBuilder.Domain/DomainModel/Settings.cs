﻿using System;
using ResumeBuilder.Utilities;

namespace ResumeBuilder.Domain.DomainModel
{
    public class Settings : Entity
    {
        public Settings()
        {
            Id = GuidComb.GenerateComb();
        }
        public Guid Id { get; set; }
        public string ApplicationName { get; set; }
        public string ApplicationUrl { get; set; }
        public bool IsClosed { get; set; }
        public bool EnableRSSFeeds { get; set; }
        public bool DisplayEditedBy { get; set; }
        public bool EnablePostFileAttachments { get; set; }
        public bool EnableMarkAsSolution { get; set; }
        public bool EnableSpamReporting { get; set; }
        public bool EnableMemberReporting { get; set; }
        public bool ManuallyAuthoriseNewMembers { get; set; }
        public bool EmailAdminOnNewMemberSignUp { get; set; }
        public bool EnableSignatures { get; set; }

        public string AdminEmailAddress { get; set; }
        public string NotificationReplyEmail { get; set; }
        public string SMTP { get; set; }
        public string SMTPUsername { get; set; }
        public string SMTPPassword { get; set; }
        public string SMTPPort { get; set; }
        public bool? SMTPEnableSSL { get; set; }

        public virtual MembershipRole NewMemberStartingRole { get; set; }
        public bool? EnableAkisment { get; set; }
        public string AkismentKey { get; set; }
        public string CurrentDatabaseVersion { get; set; }
        public string SpamQuestion { get; set; }
        public string SpamAnswer { get; set; }
        public bool? SuspendRegistration { get; set; }

        //public virtual Language DefaultLanguage { get; set; }
    }
}
