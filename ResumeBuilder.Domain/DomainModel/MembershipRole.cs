﻿using System;
using System.Collections.Generic;
using ResumeBuilder.Utilities;

namespace ResumeBuilder.Domain.DomainModel
{
    public class MembershipRole : Entity
    {
        public MembershipRole()
        {
            Id = GuidComb.GenerateComb();
        }

        public Guid Id { get; set; }
        public string RoleName { get; set; }

        public virtual IList<MembershipUser> Users { get; set; }

        public virtual Settings Settings { get; set; }
    }
}
