﻿using System;
using ResumeBuilder.Utilities;


namespace ResumeBuilder.Domain.DomainModel
{
    public class EmailTemplates : Entity
    {
        public EmailTemplates()
        {
            Id = GuidComb.GenerateComb();
        }
        public Guid Id { get; set; }
        public string EmailTemplateId { get; set; }
        public string EmailTemplateName { get; set; }
        public string EmailTemplateSubject { get; set; }
        public string EmailTemplateBody { get; set; }
        public DateTime? LastModified { get; set; }
        public bool IsActive { get; set; }
    }
}
