﻿
namespace ResumeBuilder.Domain.Constants
{
    public class AppConstants
    {
        /// <summary>
        /// A short cache time to help with speeding up the site
        /// </summary>
        public const int DefaultCacheLengthInSeconds = 600;
        public const string MessageViewBagName = "Message";
        public const string EditModelViewBagName = "EditModel";
        // Paging options
        public const int AdminListPageSize = 30;
        public const int PagingGroupSize = 10;

        /// <summary>
        /// Main roles [these should never be changed]
        /// </summary>
        public const string RoleNameStaff = "Employeer";
        public const string RoleNameUsers = "Jobseeker";
        public const string RoleNameSuperAdmin = "SuperAdmin";
        public const string RoleNameAdministrator = "Administrator";

        /// <summary>
        /// Default Date Format 
        /// </summary>
        public const string DefaultDateFormat = "dd MMM, yyyy";
        public const string DefaultDateTimeFormat = "dd MMM, yyyy hh:mm tt";

        /// <summary>
        /// Upload folder path 
        /// </summary>
        public const string UploadFolderPath = "~/Content/uploads/";
        public const string UploadThumbFolderPath = "~/Content/uploads/thumbs/";
        public const string UploadThumb50X50FolderPath = "~/Content/uploads/thumbs50x50/";
        public const string DefaultConverterThumb50X50Path = "~/Content/uploads/ConverterDefault40X40.png";
        public const string DefaultConverterThumb100X100Path = "ConverterDefault100X100.png";
        public const string ImportUploadFolderPath = "~/Content/uploads/ImportFiles/";
        public const string ExcelDownload = "~/Content/uploads/Downloads/";

        /// <summary>
        /// Member ship user last Activity Time Check 
        /// </summary>
        public const int TimeSpanInMinutesToDoCheck = 10;
        public const int TimeSpanInMinutesToShowMembers = 20;

        public const string EmailForgotPassword = "Forgot-Password";
        public const string EmailChangePassword = "Change-Password";
        public const string EmailNewUser = "New-User";

        public const string CurrentUserId = "CurrentUserId";
        public const string HistoryForCreate = "CREATE";
        public const string HistoryForUpdate = "UPDATE";
        public const string HistoryForDelete = "DELETE";
        public const string HistoryForWebAdvanceSearch = "WEB-AdvanceSearch";
        public const string HistoryForAppAdvanceSearch = "APP-AdvanceSearch";
        public const string HistoryForWebSearch = "WEB-SEARCH";
        public const string HistoryForAppSearch = "APP-SEARCH";
        public const string HistoryForWebView = "WEB-VIEW";
        public const string HistoryForAppView = "APP-VIEW";
        public const string HistorySeparator = "#&#";

        public const string RecordStatusChanged = "{0} {1} successfully";
        public const string RecordSaveException = "Error: ";
    }
}

