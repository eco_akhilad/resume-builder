﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ResumeBuilder.Domain.Exceptions
{
    public class Error
    {
        public static ArgumentNullException ArgumentNull(string parameterName)
        {
            return new ArgumentNullException(parameterName);
        }

        public static ArgumentNullException ArgumentNull(string parameterName, string messageFormat, params object[] messageArgs)
        {
            return new ArgumentNullException(parameterName, string.Format(messageFormat, messageArgs));
        }
    }
}
