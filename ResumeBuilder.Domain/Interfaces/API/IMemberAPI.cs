﻿using System;
using System.Collections.Generic;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Interfaces.API
{
    public interface IMemberAPI
    {
        /// <summary>
        /// Create a new member
        /// </summary>
        /// <param name="member"></param>
        MembershipUser Create(MembershipUser member);

        /// <summary>
        /// Delete a member
        /// </summary>
        /// <param name="member"></param>
        void Delete(MembershipUser member);

        /// <summary>
        /// Get member by id
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns>The member, or null if not found</returns>
        MembershipUser GetMemberById(Guid memberId);

    }
}
