﻿using System;
using System.Collections.Generic;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Interfaces.Services
{
    public interface IRoleService
    {
        IList<MembershipRole> AllRoles();
        void Save(MembershipRole user);
        void Delete(MembershipRole role);
        MembershipRole GetRole(string rolename);
        MembershipRole GetRole(Guid Id);
        IList<MembershipUser> GetUsersForRole(string roleName);
        void CreateRole(MembershipRole role);
    }
}
