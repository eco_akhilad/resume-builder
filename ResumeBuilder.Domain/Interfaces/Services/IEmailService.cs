﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Interfaces.Services
{
    public interface IEmailService
    {
        void SendMail(Email email);
        void SendMail(List<Email> email);
        string EmailTemplate(string to, string content);
        string EmailTemplateBody(string content);

    }
}
