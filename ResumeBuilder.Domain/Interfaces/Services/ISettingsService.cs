﻿using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces.UnitOfWork;

namespace ResumeBuilder.Domain.Interfaces.Services
{
    public interface ISettingsService
    {
        Settings GetSettings(bool useCache = true);
        void Save(Settings settings);
        Settings Add(Settings settings);
    }
}
