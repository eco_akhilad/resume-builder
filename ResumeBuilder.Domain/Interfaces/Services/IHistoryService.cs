﻿using System;
using System.Collections.Generic;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Interfaces.Services
{
    public interface IHistoryService
    {
        IList<History> GetHistoryByItem(Guid itemGuid);
        IList<History> GetHistoryByUser(Guid userGuid);
        IList<History> GetHistoryByTable(string tblName);
        History GetRecord(int Id);
        void Add(History History);
    }
}
