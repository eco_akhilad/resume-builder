﻿using ResumeBuilder.Domain.Interfaces.Services;

namespace ResumeBuilder.Domain.Interfaces.Events
{
    public interface IEventManager
    {
        /// <summary>
        /// Use reflection to get all event handling classes. Call this ONCE.
        /// </summary>
        void Initialize(ILoggingService loggingService);

    }
}
