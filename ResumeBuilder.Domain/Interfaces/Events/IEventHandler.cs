﻿using ResumeBuilder.Domain.Events;

namespace ResumeBuilder.Domain.Interfaces.Events
{
    public interface IEventHandler
    {
        void RegisterHandlers(EventManager theEventManager);
    }
}
