﻿using System;
using System.Collections.Generic;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Interfaces.Repositories
{
    public interface IRoleRepository
    {
        IList<MembershipRole> AllRoles();
        MembershipRole GetRole(string rolename);

        MembershipRole Add(MembershipRole item);
        MembershipRole Get(Guid id);
        void Delete(MembershipRole item);
        void Update(MembershipRole item);
    }
}
