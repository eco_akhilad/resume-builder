﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Interfaces.Repositories
{
    public interface IEmailTemplatesRepository
    {
        IList<EmailTemplates> AllEmailTemplates();

        EmailTemplates GetEmailById(Guid Id);
        EmailTemplates GetEmailByName(string EmailTemplateName);

        void Save(EmailTemplates EmailTemplate);
        void Delete(EmailTemplates EmailTemplate);
        void Create(EmailTemplates EmailTemplate);
    }
}
