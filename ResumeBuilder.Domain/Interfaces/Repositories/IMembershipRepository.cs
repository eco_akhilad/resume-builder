﻿using System;
using System.Collections.Generic;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Interfaces.Repositories
{
    public interface IMembershipRepository
    {
        MembershipUser GetUser(string username);
        IList<MembershipUser> SearchMembers(string username, int amount);
        IList<MembershipUser> GetActiveMembers();
        PagedList<MembershipUser> SearchMembers(string search, int pageIndex, int pageSize);
        MembershipUser GetUserBySlug(string slug);
        MembershipUser GetUserByEmail(string slug);
        IList<MembershipUser> GetUsersByRoleName(string sRoleName);
        IList<MembershipUser> GetUserBySlugLike(string slug);
        IList<MembershipUser> GetUsersById(List<Guid> guids);
        IList<MembershipUser> GetAll();
        PagedList<MembershipUser> GetAll(int pageIndex, int pageSize);
        IList<MembershipUser> GetLatestUsers(int amountToTake);
        int MemberCount();

        MembershipUser Add(MembershipUser item);
        MembershipUser Get(Guid id);
        void Delete(MembershipUser item);
        void Update(MembershipUser item);
    }
}
