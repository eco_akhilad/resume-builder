﻿using System;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Interfaces.Repositories
{
    public interface ISettingsRepository
    {
        Settings GetSettings();

        Settings Add(Settings item);
        Settings Get(Guid id);
        void Update(Settings item);
    }
}
