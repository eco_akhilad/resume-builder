﻿using System;
using System.Collections.Generic;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Interfaces.Repositories
{
    public interface IHistoryRepository
    {
        IList<History> GetHistoryByItem(Guid itemGuid);
        IList<History> GetHistoryByUser(Guid userGuid);
        IList<History> GetHistoryByTable(string tblName);
        void Add(History history);
        History GetRecord(int Id);
    }
}
