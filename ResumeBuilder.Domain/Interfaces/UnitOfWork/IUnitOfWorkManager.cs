﻿using System;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Domain.Interfaces.UnitOfWork
{
    public interface IUnitOfWorkManager : IDisposable
    {
        IUnitOfWork NewUnitOfWork();
    }
}
