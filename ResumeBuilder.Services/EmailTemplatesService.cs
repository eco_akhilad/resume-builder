﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Domain.Interfaces.Repositories;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Services
{
    public class EmailTemplatesService : IEmailTemplatesService
    {
        private readonly IEmailTemplatesRepository _EmailTemplatesRepository;
        public EmailTemplatesService(IEmailTemplatesRepository EmailTemplatesRepository)
        {
            _EmailTemplatesRepository = EmailTemplatesRepository;
        }

        public IList<EmailTemplates> AllEmailTemplates()
        {
            return _EmailTemplatesRepository.AllEmailTemplates();
        }

        public EmailTemplates GetEmailById(Guid Id)
        {
            return _EmailTemplatesRepository.GetEmailById(Id);
        }

        public EmailTemplates GetEmailByName(string EmailTemplateName)
        {
            return _EmailTemplatesRepository.GetEmailByName(EmailTemplateName);
        }

        public void Save(EmailTemplates EmailTemplate)
        {
            _EmailTemplatesRepository.Save(EmailTemplate);
        }

        public void Delete(EmailTemplates EmailTemplate)
        {
            _EmailTemplatesRepository.Delete(EmailTemplate);
        }

        public void Create(EmailTemplates EmailTemplate)
        {
            _EmailTemplatesRepository.Create(EmailTemplate);
        }

        public int GetCount()
        {
            return _EmailTemplatesRepository.AllEmailTemplates().Count;
        }
    }
}
