﻿using System;
using System.Collections.Generic;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Domain.Interfaces.Repositories;

namespace ResumeBuilder.Services
{
    public class HistoryService : IHistoryService
    {
        private readonly IHistoryRepository _historyRepository;
        public HistoryService(IHistoryRepository historyRepository)
        {
            _historyRepository = historyRepository;
        }
        public IList<History> GetHistoryByItem(Guid itemGuid)
        {
            return _historyRepository.GetHistoryByItem(itemGuid);
        }
        public IList<History> GetHistoryByUser(Guid userGuid)
        {
            return _historyRepository.GetHistoryByUser(userGuid);
        }
        public IList<History> GetHistoryByTable(string tblName)
        {
            return _historyRepository.GetHistoryByTable(tblName);
        }
        public void Add(History history)
        {
            _historyRepository.Add(history);
        }
        public History GetRecord(int Id)
        {
            return _historyRepository.GetRecord(Id);
        }
    }
}
