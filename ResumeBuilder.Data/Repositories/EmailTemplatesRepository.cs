﻿using System;
using System.Linq;
using System.Collections.Generic;
using ResumeBuilder.Domain.Interfaces.Repositories;
using ResumeBuilder.Data.Context;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces;

namespace ResumeBuilder.Data.Repositories
{
    public class EmailTemplatesRepository : IEmailTemplatesRepository
    {
        private readonly ResumeBuilderConnection _Context;
        public EmailTemplatesRepository(IResumeBuilderConnection Context)
        {
            _Context = Context as ResumeBuilderConnection;
        }

        public IList<EmailTemplates> AllEmailTemplates()
        {
            return _Context.EmailTemplates.ToList();
        }

        public EmailTemplates GetEmailById(Guid Id)
        {
            return _Context.EmailTemplates.FirstOrDefault(x => x.Id == Id);
        }

        public EmailTemplates GetEmailByName(string EmailTemplateName)
        {
            return _Context.EmailTemplates.FirstOrDefault(x => x.EmailTemplateName.ToLower().Contains(EmailTemplateName.ToLower()));
        }

        public void Save(EmailTemplates EmailTemplate)
        {
            throw new NotImplementedException();
        }

        public void Delete(EmailTemplates EmailTemplate)
        {
            _Context.EmailTemplates.Remove(EmailTemplate);
        }

        public void Create(EmailTemplates EmailTemplate)
        {
            _Context.EmailTemplates.Add(EmailTemplate);
        }
    }
}
