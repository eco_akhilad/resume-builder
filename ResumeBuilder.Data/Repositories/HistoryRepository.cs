﻿using System;
using System.Linq;
using System.Collections.Generic;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces.Repositories;
using ResumeBuilder.Data.Context;
using ResumeBuilder.Domain.Interfaces;

namespace ResumeBuilder.Data.Repositories
{
    public class HistoryRepository : IHistoryRepository
    {
        private readonly ResumeBuilderConnection _context;

        public HistoryRepository(IResumeBuilderConnection context)
        {
            _context = context as ResumeBuilderConnection;
        }
        public IList<History> GetHistoryByItem(Guid itemGuid)
        {
            return _context.History.Where(x => x.Item_Id == itemGuid).ToList();
        }
        public IList<History> GetHistoryByUser(Guid userGuid)
        {
            return _context.History.Where(x => x.User.Id == userGuid).ToList();
        }
        public IList<History> GetHistoryByTable(string tblName)
        {
            return _context.History.Where(x => x.TableName.ToLower() == tblName.ToLower()).ToList();
        }
        public void Add(History salesTarget)
        {
            _context.History.Add(salesTarget);
        }

        public History GetRecord(int Id)
        {
            return _context.History.FirstOrDefault(x => x.Id == Id);
        }
    }
}
