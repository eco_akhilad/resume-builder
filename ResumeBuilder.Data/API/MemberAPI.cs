﻿using System;
using System.Collections.Generic;
using ResumeBuilder.Domain.Interfaces.API;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces.Repositories;

namespace ResumeBuilder.API
{
    public class MemberAPI : IMemberAPI
    {
        private readonly IMembershipRepository _membershipRepository;

        public MemberAPI(IMembershipRepository membershipRepository)
        {
            _membershipRepository = membershipRepository;
        }

        public MembershipUser GetMemberById(Guid memberId)
        {
            return _membershipRepository.Get(memberId);
        }

        //public IList<MembershipUserPoints> GetMembersPoints(Guid memberId)
        //{
        //    return _membershipRepository.Get(memberId).Points;
        //}

        public MembershipUser Create(MembershipUser member)
        {
            return _membershipRepository.Add(member);
        }

        public void Delete(MembershipUser member)
        {
            _membershipRepository.Delete(member);
        }
    }
}
