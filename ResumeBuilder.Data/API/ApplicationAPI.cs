﻿using ResumeBuilder.Domain.Interfaces.API;

namespace ResumeBuilder.API
{
    public class ApplicationAPI : IApplicationAPI
    {
        /// <summary>
        /// Constructor
        /// </summary>
        //public ApplicationAPI(IMemberAPI memberAPI, IPostAPI postAPI, ITopicAPI topicAPI, ICategoryAPI categoryAPI)
        //{
        //    Member = memberAPI;
        //    Post = postAPI;
        //    Topic = topicAPI;
        //    Category = categoryAPI;
        //}
        public ApplicationAPI(IMemberAPI memberAPI)
        {
            Member = memberAPI;
        }

        public IMemberAPI Member { get; set; }
        //public IPostAPI Post { get; set; }
        //public ITopicAPI Topic { get; set; }
        //public ICategoryAPI Category { get; set; }
    }
}
