﻿using System.Data.Entity.ModelConfiguration;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Data.Mapping
{
    class HistoryMapping : EntityTypeConfiguration<History>
    {
        public HistoryMapping()
        {
            HasKey(x => x.Id);
        }
    }
}
