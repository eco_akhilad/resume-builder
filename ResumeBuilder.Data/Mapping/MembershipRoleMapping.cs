﻿using System.Data.Entity.ModelConfiguration;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Data.Mapping
{
    public class MembershipRoleMapping : EntityTypeConfiguration<MembershipRole>
    {
        public MembershipRoleMapping()
        {
            HasKey(x => x.Id);

        }
    }
}
