﻿using System.Data.Entity.ModelConfiguration;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Data.Mapping
{
    public class SettingsMapping : EntityTypeConfiguration<Settings>
    {
        public SettingsMapping()
        {
            HasKey(x => x.Id);

            HasRequired(t => t.NewMemberStartingRole)
                .WithOptional(x => x.Settings).Map(m => m.MapKey("NewMemberStartingRole"));

        }
    }
}
