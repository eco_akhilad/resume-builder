﻿using System.Data.Entity.ModelConfiguration;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Data.Mapping
{
    public class EmailTemplatesMapping : EntityTypeConfiguration<EmailTemplates>
    {
        public EmailTemplatesMapping()
        {
            HasKey(x => x.Id);
        }
    }
}
