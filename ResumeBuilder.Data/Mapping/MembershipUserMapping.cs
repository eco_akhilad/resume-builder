﻿using System.Data.Entity.ModelConfiguration;
using ResumeBuilder.Domain.DomainModel;

namespace ResumeBuilder.Data.Mapping
{
    public class MembershipUserMapping : EntityTypeConfiguration<MembershipUser>
    {
        public MembershipUserMapping()
        {
            HasKey(x => x.Id);

            // Many-to-many join table - a user may belong to many roles
            HasMany(t => t.Roles)
            .WithMany(t => t.Users)
            .Map(m =>
            {
                m.ToTable("MembershipUsersInRoles");
                m.MapLeftKey("UserIdentifier");
                m.MapRightKey("RoleIdentifier");
            });

            HasMany(x => x.Historys)
                .WithRequired(x => x.User)
                .Map(x => x.MapKey("User_Id"));

        }
    }
}
