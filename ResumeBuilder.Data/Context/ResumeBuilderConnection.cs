﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using ResumeBuilder.Data.Mapping;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces;

namespace ResumeBuilder.Data.Context
{
    public class ResumeBuilderConnection : DbContext, IResumeBuilderConnection
    {
        public const string DatabaseConnectionName = @"ResumeBuilderConnection";

        // http://blogs.msdn.com/b/adonet/archive/2010/12/06/ef-feature-ctp5-fluent-api-samples.aspx
        public DbSet<EmailTemplates> EmailTemplates { get; set; }
        public DbSet<MembershipRole> MembershipRole { get; set; }
        public DbSet<MembershipUser> MembershipUser { get; set; }
        public DbSet<Settings> Setting { get; set; }
        public DbSet<History> History { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public ResumeBuilderConnection()
            : base(DatabaseConnectionName)
        //: base(CreateCachingConnection(DatabaseConnectionName), true)
        {
            Configuration.LazyLoadingEnabled = true;
        }

        /// <summary>
        /// Creates tracing connection which is a wrapper around native store connection.
        /// </summary>
        /// <returns>Instance of <see cref="EFCachingConnection"/> with enabled caching.</returns>
        //private static EFCachingConnection CreateCachingConnection(string connectionName)
        //{
        //    // Based on the tracing example at: http://jkowalski.com/2010/04/23/logging-sql-statements-in-entity-frameworkcode-first/
        //    var connection = System.Configuration.ConfigurationManager.ConnectionStrings[connectionName];

        //    var efCachingconnection = new EFCachingConnection
        //    {
        //        ConnectionString = @"wrappedProvider=" + connection.ProviderName + ";" + connection.ConnectionString
        //    };

        //    return efCachingconnection;
        //}

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // http://stackoverflow.com/questions/7924758/entity-framework-creates-a-plural-table-name-but-the-view-expects-a-singular-ta
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            // Mappings
            modelBuilder.Configurations.Add(new EmailTemplatesMapping());
            modelBuilder.Configurations.Add(new SettingsMapping());
            modelBuilder.Configurations.Add(new MembershipRoleMapping());
            modelBuilder.Configurations.Add(new MembershipUserMapping());


            // Ignore properties on domain models
            //modelBuilder.Entity<Category>().Ignore(cat => cat.SubCategories);

            base.OnModelCreating(modelBuilder);
        }

        public new void Dispose()
        {

        }
    }
}
