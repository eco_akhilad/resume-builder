using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using ResumeBuilder.Domain.Interfaces;
using ResumeBuilder.Data.Context;
using ResumeBuilder.Domain.Interfaces.UnitOfWork;
using ResumeBuilder.Data.UnitOfWork;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Services;
using ResumeBuilder.Domain.Interfaces.Repositories;
using ResumeBuilder.Data.Repositories;
using ResumeBuilder.Domain.Interfaces.API;
using ResumeBuilder.API;

namespace ResumeBuilder.IoC
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = BuildUnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }

        /// <summary>
        /// Inject
        /// </summary>
        /// <returns></returns>
        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // Database context, one per request, ensure it is disposed
            container.BindInRequestScope<IResumeBuilderConnection, ResumeBuilderConnection>();
            container.BindInRequestScope<IUnitOfWorkManager, UnitOfWorkManager>();

            //Bind the various domain model services that e.g. our controllers require
            container.BindInRequestScope<IIntegrityServiceManager, IntegrityServiceManager>();
            container.BindInRequestScope<ILoggingService, LoggingService>();
            container.BindInRequestScope<IHistoryService, HistoryService>();
            container.BindInRequestScope<IEmailService, EmailService>();
            container.BindInRequestScope<IRoleService, RoleService>();
            container.BindInRequestScope<IMembershipService, MembershipService>();
            container.BindInRequestScope<ISettingsService, SettingsService>();
            container.BindInRequestScope<IEmailTemplatesService, EmailTemplatesService>();


            //Bind the various domain model repositories that e.g. our services require   
            container.BindInRequestScope<IHistoryRepository, HistoryRepository>();
            container.BindInRequestScope<IRoleRepository, RoleRepository>();
            container.BindInRequestScope<IMembershipRepository, MembershipRepository>();
            container.BindInRequestScope<ISettingsRepository, SettingsRepository>();
            container.BindInRequestScope<IEmailTemplatesRepository, EmailTemplatesRepository>();


            container.BindInRequestScope<IApplicationAPI, ApplicationAPI>();
            container.BindInRequestScope<IMemberAPI, MemberAPI>();
            //container.BindInRequestScope<ISessionHelper, SessionHelper>();

            return container;
        }
    }


    // http://weblogs.asp.net/shijuvarghese/archive/2010/05/07/dependency-injection-in-asp-net-mvc-nerddinner-app-using-unity-2-0.aspx
    /// <summary>
    /// Bind the given interface in request scope
    /// </summary>
    public static class IOCExtensions
    {
        public static void BindInRequestScope<T1, T2>(this IUnityContainer container) where T2 : T1
        {
            container.RegisterType<T1, T2>(new HierarchicalLifetimeManager());
        }

        public static void BindInSingletonScope<T1, T2>(this IUnityContainer container) where T2 : T1
        {
            container.RegisterType<T1, T2>(new ContainerControlledLifetimeManager());
        }
    }
}