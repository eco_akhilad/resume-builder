﻿using System.Data;
using System.Data.OleDb;

namespace ResumeBuilder.Web.Application
{
    public class ExcelConnectionsObject
    {
        public ExcelConnectionsObject()
        {

        }
        public static OleDbConnection getExcelConnection(string filePath)
        {
            return new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + filePath + ";Extended Properties='Excel 12.0 XML;HDR=Yes;'");
        }
        public static OleDbCommand getExcelCommand(string oleCommand, OleDbConnection oleConn)
        {
            return new OleDbCommand(oleCommand, oleConn);
        }
        public DataTable getExcelData(string filePath, int iSheetIndex)
        {
            OleDbConnection olaConn = getExcelConnection(filePath);
            olaConn.Open();

            string sheetName = getExcelSheetName(olaConn, iSheetIndex);

            OleDbDataAdapter olaData = new OleDbDataAdapter(getExcelCommand("select * from [" + sheetName + "]", olaConn));
            DataTable olaDataTable = new DataTable();
            olaData.Fill(olaDataTable);

            olaConn.Close();
            return olaDataTable;
        }
        public static void Resort(ref DataTable dt, string colName, string direction)
        {
            string sortExpression = string.Format("{0} {1}", colName, direction);
            dt.DefaultView.Sort = sortExpression;
            dt = dt.DefaultView.ToTable();
        }
        public OleDbDataReader getExcelData(string filePath)
        {
            OleDbConnection olaConn = getExcelConnection(@"Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + filePath + ";Extended Properties=Excel 8.0");
            olaConn.Open();
            string sheetName = getExcelSheetName(olaConn);
            OleDbCommand ocmd = getExcelCommand("select * from [" + sheetName + "]", olaConn);
            return ocmd.ExecuteReader();
        }
        public static string getExcelSheetName(OleDbConnection oleConn)
        {
            DataTable dt = oleConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            if (dt.Rows.Count > 0)
                return dt.Rows[0]["TABLE_NAME"].ToString();
            else
                return "";
        }
        public static string getExcelSheetName(OleDbConnection oleConn, int iSheetIndex)
        {
            DataTable dt = oleConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

            if (dt.Rows.Count >= iSheetIndex)
                return dt.Rows[iSheetIndex]["TABLE_NAME"].ToString();
            else
                return "";
        }
    }
}