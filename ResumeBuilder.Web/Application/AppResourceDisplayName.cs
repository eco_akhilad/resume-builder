﻿using System.ComponentModel;
using System.Web.Mvc;
using ResumeBuilder.Domain.Interfaces;
using ResumeBuilder.Domain.Interfaces.Services;
using System.Resources;
using System.Reflection;

namespace ResumeBuilder.Web.Application
{
    public class AppResourceDisplayName : DisplayNameAttribute, IModelAttribute
    {
        private string _resourceValue = string.Empty;

        public AppResourceDisplayName(string resourceKey)
            : base(resourceKey)
        {
            ResourceKey = resourceKey;
        }

        public string ResourceKey { get; set; }

        public override string DisplayName
        {
            get
            {
                //var localizationService = DependencyResolver.Current.GetService<ILocalizationService>();
                //_resourceValue = localizationService.GetResourceString(ResourceKey.Trim());
                //return _resourceValue;

                //Assembly assembly = this.GetType().Assembly;
                //ResourceManager resourceManager = new ResourceManager("Resources", assembly);
                //return _resourceValue = resourceManager.GetString(ResourceKey.Trim());
                return ResourceKey;
            }
        }

        public string Name
        {
            get { return "ResourceDisplayName"; }
        }

    }
}

