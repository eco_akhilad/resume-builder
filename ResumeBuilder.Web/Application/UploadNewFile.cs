﻿using System;
using System.Linq;
using System.Web;
using System.IO;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Domain.Interfaces.UnitOfWork;
using ResumeBuilder.Utilities;
using System.Web.Mvc;
using ResumeBuilder.Domain.Constants;
using ResumeBuilder.ViewModels;
using ResumeBuilder.Web.Controllers;

namespace ResumeBuilder.Web.Application
{
    public class UploadNewFile : BaseController
    {
        public UploadNewFile()
            : base(DependencyResolver.Current.GetService<ILoggingService>(),
                DependencyResolver.Current.GetService<IUnitOfWorkManager>(),
                DependencyResolver.Current.GetService<IMembershipService>(),
                DependencyResolver.Current.GetService<IRoleService>(),
                DependencyResolver.Current.GetService<ISettingsService>()) { }

        public GenericMessageViewModel UploadFiles(HttpPostedFileBase file, Guid DocId)
        {
            try
            {
                var uploadFolderPath = System.Web.HttpContext.Current.Server.MapPath(string.Concat(AppConstants.UploadFolderPath, LoggedOnUser.Id));
                var uploadThumbFolderPath = System.Web.HttpContext.Current.Server.MapPath(string.Concat(AppConstants.UploadThumbFolderPath, LoggedOnUser.Id));

                if (!Directory.Exists(uploadFolderPath))
                {
                    Directory.CreateDirectory(uploadFolderPath);
                }
                if (!Directory.Exists(uploadThumbFolderPath))
                {
                    Directory.CreateDirectory(uploadThumbFolderPath);
                }

                if (file != null)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    if (fileName != null)
                    {
                        //Before we do anything, check file size
                        if (file.ContentLength > Convert.ToInt32(ConfigUtils.GetAppSetting("FileUploadMaximumSizeInBytes")))
                        {
                            //File is too big
                            return new GenericMessageViewModel
                            {
                                Message = LocalizationService.GetResourceString("Max file upload size is 5MB."),
                                MessageType = GenericMessages.error
                            };
                        }

                        // now check allowed extensions
                        var allowedFileExtensions = ConfigUtils.GetAppSetting("FileUploadAllowedExtensions");

                        // Get the file extension
                        var fileExtension = Path.GetExtension(file.FileName.ToLower());
                        if (!string.IsNullOrEmpty(allowedFileExtensions))
                        {
                            // Turn into a list and strip unwanted commas as we don't trust users!
                            var allowedFileExtensionsList = allowedFileExtensions.ToLower().Trim()
                                                             .Trim(',').Split(',').ToList();


                            // If can't work out extension then just error
                            if (string.IsNullOrEmpty(fileExtension))
                            {
                                return new GenericMessageViewModel
                                {
                                    Message = LocalizationService.GetResourceString("Errors.GenericMessage"),
                                    MessageType = GenericMessages.error
                                };
                            }

                            // Remove the dot then check against the extensions in the web.config settings
                            fileExtension = fileExtension.TrimStart('.');
                            if (!allowedFileExtensionsList.Contains(fileExtension))
                            {
                                // File extension now allowed
                                return new GenericMessageViewModel
                                {
                                    Message = LocalizationService.GetResourceString("This extension are not allowed."),
                                    MessageType = GenericMessages.error
                                };
                            }
                        }

                        // Sort the file name
                        // var newFileName = string.Format("{0}_{1}", GuidComb.GenerateComb(), file.FileName.Trim(' ').Replace("_", "-").Replace(" ", "-").ToLower());
                        var ss = GuidComb.GenerateComb().ToString().Split('-');


                        var OnlyFilename = Path.GetFileName(file.FileName);
                        var newFileName = string.Format("{0}_{1}", ss[0], OnlyFilename.Trim(' ').Replace("_", "-").Replace(" ", "-").ToLower());

                        var path = Path.Combine(uploadFolderPath, newFileName);
                        var thumbPath = Path.Combine(uploadThumbFolderPath, newFileName.Replace(".pdf", ".jpg"));

                        // Save the file to disk
                        file.SaveAs(path);
                        //if (Pdf2Image.ImageUtil.IsPDF(path))
                        //{
                        //    int pageNum = 1;
                        //    bool isSaved = Pdf2Image.ASPPDFLib.GetPageImage(path, thumbPath, pageNum, 99);
                        //    //string savedPath = Pdf2Image.ASPPDFLib.GetPageFromPDF(path, thumbPath, ref pageNum, 150);
                        //}
                        return new GenericMessageViewModel
                        {
                            Message = LoggedOnUser.Id + "/" + newFileName,
                            MessageType = GenericMessages.success
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                // Redirect to the topic with a success message
                LoggingService.Error(ex);
            }
            return new GenericMessageViewModel
            {
                Message = LocalizationService.GetResourceString("Errors.GenericMessage") + " in file upload.",
                MessageType = GenericMessages.error
            };
        }

        public GenericMessageViewModel UploadFile(HttpPostedFileBase file, Guid DocId)
        {
            try
            {
                var uploadFolderPath = System.Web.HttpContext.Current.Server.MapPath(string.Concat(AppConstants.UploadFolderPath, LoggedOnUser.Id));
                var uploadThumbFolderPath = System.Web.HttpContext.Current.Server.MapPath(string.Concat(AppConstants.UploadThumbFolderPath, LoggedOnUser.Id));

                if (!Directory.Exists(uploadFolderPath))
                {
                    Directory.CreateDirectory(uploadFolderPath);
                }
                if (!Directory.Exists(uploadThumbFolderPath))
                {
                    Directory.CreateDirectory(uploadThumbFolderPath);
                }

                if (file != null)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    if (fileName != null)
                    {
                        //Before we do anything, check file size
                        if (file.ContentLength > Convert.ToInt32(ConfigUtils.GetAppSetting("FileUploadMaximumSizeInBytes")))
                        {
                            //File is too big
                            return new GenericMessageViewModel
                            {
                                Message = LocalizationService.GetResourceString("Post.UploadFileTooBig"),
                                MessageType = GenericMessages.error
                            };
                        }

                        // now check allowed extensions
                        var allowedFileExtensions = ConfigUtils.GetAppSetting("FileUploadAllowedForBS");

                        // Get the file extension
                        var fileExtension = Path.GetExtension(file.FileName.ToLower());
                        if (!string.IsNullOrEmpty(allowedFileExtensions))
                        {
                            // Turn into a list and strip unwanted commas as we don't trust users!
                            var allowedFileExtensionsList = allowedFileExtensions.ToLower().Trim()
                                                             .Trim(',').Split(',').ToList();

                            // If can't work out extension then just error
                            if (string.IsNullOrEmpty(fileExtension))
                            {
                                return new GenericMessageViewModel
                                {
                                    Message = LocalizationService.GetResourceString("Errors.GenericMessage"),
                                    MessageType = GenericMessages.error
                                };
                            }

                            // Remove the dot then check against the extensions in the web.config settings
                            fileExtension = fileExtension.TrimStart('.');
                            if (!allowedFileExtensionsList.Contains(fileExtension))
                            {
                                // File extension now allowed
                                return new GenericMessageViewModel
                                {
                                    Message = LocalizationService.GetResourceString("Post.UploadBannedFileExtension"),
                                    MessageType = GenericMessages.error
                                };
                            }
                        }

                        // Sort the file name
                        // var newFileName = string.Format("{0}_{1}", GuidComb.GenerateComb(), file.FileName.Trim(' ').Replace("_", "-").Replace(" ", "-").ToLower());
                        var ss = GuidComb.GenerateComb().ToString().Split('-');


                        var OnlyFilename = Path.GetFileName(file.FileName);
                        var newFileName = string.Format("{0}_{1}", ss[0], OnlyFilename.Trim(' ').Replace("_", "-").Replace(" ", "-").ToLower());

                        var path = Path.Combine(uploadFolderPath, newFileName);
                        var thumbPath = Path.Combine(uploadThumbFolderPath, newFileName.Replace(".pdf", ".jpg"));

                        // Save the file to disk
                        file.SaveAs(path);

                        //if (Pdf2Image.ImageUtil.IsPDF(path))
                        //{
                        //    int pageNum = 1;
                        //    bool isSaved = Pdf2Image.ASPPDFLib.GetPageImage(path, thumbPath, pageNum, 99);
                        //}

                        return new GenericMessageViewModel
                        {
                            Message = LoggedOnUser.Id + "/" + newFileName,
                            MessageType = GenericMessages.success
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                // Redirect to the topic with a success message
                LoggingService.Error(ex);
            }
            return new GenericMessageViewModel
            {
                Message = LocalizationService.GetResourceString("Errors.GenericMessage") + " in file upload.",
                MessageType = GenericMessages.error
            };
        }
    }
}