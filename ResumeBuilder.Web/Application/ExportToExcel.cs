﻿using System.IO;
using System.Web;
using System.Data;
using System.Web.Mvc;
using ClosedXML.Excel;
using System.Web.UI.WebControls;

namespace ResumeBuilder.Web.Application
{
    public class ExportToExcel : ActionResult
    {
        public GridView ExcelGridView { get; set; }
        public string fileName { get; set; }

        public ExportToExcel(GridView gv, string pFileName)
        {
            ExcelGridView = gv;
            fileName = pFileName;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            DataTable dt = new DataTable("GridView_Data");
            foreach (TableCell cell in ExcelGridView.HeaderRow.Cells)
            {
                dt.Columns.Add(cell.Text);
            }
            foreach (GridViewRow row in ExcelGridView.Rows)
            {
                dt.Rows.Add();
                for (int i = 0; i < row.Cells.Count; i++)
                {
                    dt.Rows[dt.Rows.Count - 1][i] = row.Cells[i].Text.ToLower() != "&nbsp;".ToLower() ? row.Cells[i].Text : "";
                }
            }
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName);
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();

                }
            }
        }
    }
}
