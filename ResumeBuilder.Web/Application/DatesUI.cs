﻿using System;
using System.Web.Mvc;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Utilities;
using System.Text.RegularExpressions;

namespace ResumeBuilder.Web.Application
{
    public static class DatesUI
    {

        private static string GetLocalisedText(string key)
        {
            //var locService = DependencyResolver.Current.GetService<ILocalizationService>();
            //return locService.GetResourceString(key);
            return key;
        }

        /// <summary>
        /// Get Current Date and Time
        /// </summary>
        /// <returns>The date, or a date representing now.</returns>
        public static DateTime getDateTime()
        {
            return DateTime.Now;
        }
        public static string FormatDateTime(DateTime? date, string format)
        {
            if (date.HasValue)
            {
                return DateTime.Parse(date.Value.ToString()).ToString(format);
            }
            return System.DateTime.Now.ToString(format);
        }
        public static string FormatStringDateTime(string date, string format)
        {
            try
            {
                return DateTime.Parse(date).ToString(format);
            }
            catch
            {
                return date.ToString();
            }
        }

        /// <summary>
        /// Returns a pretty date like FB
        /// </summary>
        /// <param name="date"></param>
        /// <returns>28 Days Ago</returns>
        public static string GetPrettyDate(string date)
        {
            DateTime time;
            if (DateTime.TryParse(date, out time))
            {
                var span = DateTime.Now.Subtract(time);
                var totalDays = (int)span.TotalDays;
                var totalSeconds = (int)span.TotalSeconds;
                if ((totalDays < 0) || (totalDays >= 0x1f))
                {
                    return DateUtils.FormatDateTime(date, "dd MMMM yyyy");
                }
                if (totalDays == 0)
                {
                    if (totalSeconds < 60)
                    {
                        return GetLocalisedText("Date.JustNow");
                    }
                    if (totalSeconds < 120)
                    {
                        return GetLocalisedText("Date.OneMinuteAgo");
                    }
                    if (totalSeconds < 0xe10)
                    {
                        return string.Format(GetLocalisedText("Date.MinutesAgo"), Math.Floor((double)(((double)totalSeconds) / 60.0)));
                    }
                    if (totalSeconds < 0x1c20)
                    {
                        return GetLocalisedText("Date.OneHourAgo");
                    }
                    if (totalSeconds < 0x15180)
                    {
                        return string.Format(GetLocalisedText("Date.HoursAgo"), Math.Floor((double)(((double)totalSeconds) / 3600.0)));
                    }
                }
                if (totalDays == 1)
                {
                    return GetLocalisedText("Date.Yesterday");
                }
                if (totalDays < 7)
                {
                    return string.Format(GetLocalisedText("Date.DaysAgo"), totalDays);
                }
                if (totalDays < 0x1f)
                {
                    return string.Format(GetLocalisedText("Date.WeeksAgo"), Math.Ceiling((double)(((double)totalDays) / 7.0)));
                }
            }
            return date;
        }
        public static string GetTimestamp(DateTime value)
        {
            return value.ToString("yyMMddHHmmssfff");
        }
        //public static DateTime GetTimeStampDate(string value)
        //{
        //    return value.ToString("yyMMddHHmmssfff");
        //}
    }
}