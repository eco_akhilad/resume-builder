﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ResumeBuilder.Domain.Constants;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Domain.Interfaces.UnitOfWork;
using ResumeBuilder.Web.Controllers;
using ResumeBuilder.Utilities;
using ResumeBuilder.ViewModels;
using System.Net.Mail;


namespace ResumeBuilder.Web.Application
{
    public class EmailHelper : ResumeBuilder.Web.Controllers.BaseController
    {
        private readonly IEmailService _emailService;
        private readonly IEmailTemplatesService _emailtemplatesService;

        public EmailHelper(ILoggingService loggingService, IUnitOfWorkManager unitOfWorkManager,
           IMembershipService membershipService, IRoleService roleService, ISettingsService settingsService,
           IEmailService emailService, IEmailTemplatesService emailtemplatesService)
            : base(loggingService, unitOfWorkManager, membershipService, roleService, settingsService)
        {

            _emailService = emailService;
            _emailtemplatesService = emailtemplatesService;

        }

        public EmailHelper()
            : base(DependencyResolver.Current.GetService<ILoggingService>(),
                DependencyResolver.Current.GetService<IUnitOfWorkManager>(),
                DependencyResolver.Current.GetService<IMembershipService>(),
                DependencyResolver.Current.GetService<IRoleService>(),
                DependencyResolver.Current.GetService<ISettingsService>())
        {
            _emailService = DependencyResolver.Current.GetService<IEmailService>();
            _emailtemplatesService = DependencyResolver.Current.GetService<IEmailTemplatesService>();

        }

        public GenericMessageViewModel SendMailOnChangePassword(MembershipUser currentUser)
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                try
                {
                    var emailtemp = _emailtemplatesService.GetEmailByName(AppConstants.EmailChangePassword);

                    if (emailtemp == null)
                    {
                        return new GenericMessageViewModel
                        {
                            Message = "Error Occurred while accessing Template",
                            MessageType = GenericMessages.error
                        };
                    }
                    if (emailtemp.IsActive != true)
                    {
                        return new GenericMessageViewModel
                        {
                            Message = "Change-Password Email Template is InActive",
                            MessageType = GenericMessages.error
                        };
                    }
                    var sb = new StringBuilder();
                    sb.Append(emailtemp.EmailTemplateBody);
                    sb.Replace("#URL#", SettingsService.GetSettings().ApplicationUrl);

                    var email = new Email
                    {
                        EmailFrom = SettingsService.GetSettings().NotificationReplyEmail,
                        EmailTo = currentUser.Email,
                        NameTo = currentUser.UserName,
                        Subject = emailtemp.EmailTemplateSubject
                    };

                    try
                    {

                        email.Body = _emailService.EmailTemplateBody(sb.ToString());
                        var CurrentContext = System.Web.HttpContext.Current;
                        new System.Threading.Thread(() =>
                        {
                            System.Web.HttpContext.Current = CurrentContext;
                            _emailService.SendMail(email);
                        }).Start();

                        // We use temp data because we are doing a redirect
                        return new GenericMessageViewModel
                        {
                            Message = "Change-Password mail sent successfully",
                            MessageType = GenericMessages.success
                        };

                    }
                    catch (Exception ex)
                    {
                        LoggingService.Error(ex);
                        return new GenericMessageViewModel
                        {
                            Message = "Error in sending Change-Password mail.",
                            MessageType = GenericMessages.error
                        };
                    }
                }

                catch (Exception ex)
                {
                    LoggingService.Error(ex);
                    return new GenericMessageViewModel
                        {
                            Message = "Error Occurred",
                            MessageType = GenericMessages.error
                        };
                }
            }
        }

        public GenericMessageViewModel SendMailOnForgotPassword(MembershipUser currentUser, string newPassword)
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                try
                {
                    var emailtemp = _emailtemplatesService.GetEmailByName(AppConstants.EmailForgotPassword);

                    if (emailtemp == null)
                    {
                        return new GenericMessageViewModel
                        {
                            Message = "Error Occurred while accessing Template",
                            MessageType = GenericMessages.error
                        };
                    }
                    if (emailtemp.IsActive != true)
                    {
                        return new GenericMessageViewModel
                        {
                            Message = "Forgot-Password Email Template is InActive",
                            MessageType = GenericMessages.error
                        };
                    }
                    var sb = new StringBuilder();
                    sb.Append(emailtemp.EmailTemplateBody);
                    sb.Replace("#USER#", currentUser.UserName);
                    sb.Replace("#Password#", newPassword);
                    sb.Replace("#URL#", SettingsService.GetSettings().ApplicationUrl);

                    var email = new Email
                    {
                        EmailFrom = SettingsService.GetSettings().NotificationReplyEmail,
                        EmailTo = currentUser.Email,
                        NameTo = currentUser.UserName,
                        Subject = emailtemp.EmailTemplateSubject
                    };

                    try
                    {

                        email.Body = _emailService.EmailTemplateBody(sb.ToString());
                        var CurrentContext = System.Web.HttpContext.Current;
                        new System.Threading.Thread(() =>
                        {
                            System.Web.HttpContext.Current = CurrentContext;
                            _emailService.SendMail(email);
                        }).Start();

                        // We use temp data because we are doing a redirect
                        return new GenericMessageViewModel
                        {
                            Message = "Password reset mail sent successfully",
                            MessageType = GenericMessages.success
                        };

                    }
                    catch (Exception ex)
                    {
                        LoggingService.Error(ex);
                        return new GenericMessageViewModel
                        {
                            Message = "Error in sending Forgot-Password mail.",
                            MessageType = GenericMessages.error
                        };
                    }
                }

                catch (Exception ex)
                {
                    LoggingService.Error(ex);
                    return new GenericMessageViewModel
                        {
                            Message = "Error Occurred",
                            MessageType = GenericMessages.error
                        };
                }
            }
        }

    }
}