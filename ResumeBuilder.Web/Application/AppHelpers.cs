﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using ResumeBuilder.Domain.Constants;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Utilities;

namespace ResumeBuilder.Web.Application
{
    public static class AppHelpers
    {
        #region FileDelete

        /// <summary>
        /// Delete File If Exists
        /// </summary>
        public static void DeleteFile(string sFilePath)
        {
            if (!string.IsNullOrEmpty(sFilePath))
                if (System.IO.File.Exists(sFilePath))
                    System.IO.File.Delete(sFilePath);
        }

        #endregion

        #region String

        public static string GetExceptionMessage(Exception ex)
        {
            if (ex.Message.ToLower().Contains("inner exception"))
            {
                if (ex.InnerException.Message.ToLower().Contains("inner exception"))
                    return ex.InnerException.InnerException.Message.Replace("\"", "").Replace("'", "");

                return ex.InnerException.Message.Replace("\"", "").Replace("'", "");
            }
            return ex.Message.Replace("\"", "").Replace("'", "");
        }

        public static string NameToPassword(string Name)
        {
            System.Text.RegularExpressions.Regex rgx = new System.Text.RegularExpressions.Regex("[^a-zA-Z0-9 -]");
            Name = rgx.Replace(Name, "");
            Name = Name.Replace(" ", "");
            if (Name.Length >= 6) Name = Name.Substring(0, 6);

            return Name;
        }

        public static string GetNewId(string append, int iNumbert)
        {
            string retVal = "";

            if (iNumbert.ToString().Length == 1)
                retVal = append + "000" + iNumbert;

            else if (iNumbert.ToString().Length == 2)
                retVal = append + "00" + iNumbert;

            else if (iNumbert.ToString().Length == 3)
                retVal = append + "0" + iNumbert;

            else
                retVal = append + "" + iNumbert;

            return retVal;
        }

        public static Guid StringToGuid(string sGuid)
        {
            try
            {
                if (!string.IsNullOrEmpty(sGuid))
                {
                    return new Guid(sGuid);
                }
            }
            catch
            {

            }
            return new Guid();
        }

        #endregion


        public static bool IsGuidNullOrEmpty(Guid? guid)
        {
            if (guid.HasValue)
                if (guid.Value != Guid.Empty)
                    return false;

            return true;
        }

        public static double GetStringToDouble(string sNumber)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(sNumber))
                {
                    if (sNumber.Count(x => x == ',') == 1)
                    {
                        return Convert.ToDouble(sNumber.Trim(), System.Threading.Thread.CurrentThread.CurrentCulture.NumberFormat);
                    }
                    else if (sNumber.Count(x => x == ',') >= 0 && sNumber.Count(x => x == '.') == 1)
                    {
                        return Convert.ToDouble(sNumber.Trim());
                    }
                    return double.Parse(sNumber.Trim());
                }
            }
            catch
            {
                try
                {
                    return double.Parse(sNumber.Trim());
                }
                catch
                { }
            }
            return 0;
        }

    }
}