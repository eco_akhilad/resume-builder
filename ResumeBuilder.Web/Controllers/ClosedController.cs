﻿using System.Web.Mvc;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Domain.Interfaces.UnitOfWork;

namespace ResumeBuilder.Web.Controllers
{
    public class ClosedController : BaseController
    {
        public ClosedController(ILoggingService loggingService, IUnitOfWorkManager unitOfWorkManager, IMembershipService membershipService,
            IRoleService roleService, ISettingsService settingsService) :
            base(loggingService, unitOfWorkManager, membershipService, roleService, settingsService)
        {
        }

        /// <summary>
        /// This is called when the forum is closed
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            System.Web.Security.FormsAuthentication.SignOut();
            return View();
        }

        /// <summary>
        /// This is called when the Page Not Found
        /// </summary>
        /// <returns></returns>
        public ActionResult PageNotFound()
        {
            return View();
        }

        /// <summary>
        /// This is called when the Error on Page 
        /// </summary>
        /// <returns></returns>
        public ActionResult Error()
        {
            return View();
        }

    }
}
