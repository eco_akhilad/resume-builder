﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ResumeBuilder.Domain.Constants;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Domain.Interfaces.UnitOfWork;
using ResumeBuilder.Utilities;
using System.Web.Security;

namespace ResumeBuilder.Web.Controllers
{
    public class HomeController : BaseController
    {

        public HomeController(ILoggingService loggingService, IUnitOfWorkManager unitOfWorkManager,
            IMembershipService membershipService, IRoleService roleService, ISettingsService settingsService)
            : base(loggingService, unitOfWorkManager, membershipService, roleService, settingsService)
        {

        }

        public ActionResult Index()
        {
            return View();
        }
    }
}
