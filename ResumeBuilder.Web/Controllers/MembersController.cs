﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using ResumeBuilder.Domain.Constants;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Domain.Interfaces.UnitOfWork;
using ResumeBuilder.Utilities;
using ResumeBuilder.ViewModels;
using MembershipCreateStatus = ResumeBuilder.Domain.DomainModel.MembershipCreateStatus;
using MembershipUser = ResumeBuilder.Domain.DomainModel.MembershipUser;
using ResumeBuilder.Web.Application;

namespace ResumeBuilder.Web.Controllers
{
    public class MembersController : BaseController
    {
        private readonly IEmailService _emailService;

        //private MembershipUser LoggedOnUser;
        //private MembershipRole UsersRole;

        //private InMemoryTokenManager _tokenManager;

        public MembersController(ILoggingService loggingService, IUnitOfWorkManager unitOfWorkManager, IMembershipService membershipService,
            IRoleService roleService, ISettingsService settingsService, IEmailService emailService)
            : base(loggingService, unitOfWorkManager, membershipService, roleService, settingsService)
        {
            _emailService = emailService;

            //LoggedOnUser = UserIsAuthenticated ? MembershipService.GetUser(UserName) : null;
            //UsersRole = LoggedOnUser == null ? RoleService.GetRole(AppConstants.GuestRoleName) : LoggedOnUser.Roles.FirstOrDefault();
        }

        [ChildActionOnly]
        public PartialViewResult GetCurrentActiveMembers()
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                var viewModel = new ActiveMembersViewModel
                {
                    ActiveMembers = MembershipService.GetActiveMembers()
                };
                return PartialView(viewModel);
            }
        }

        public JsonResult LastActiveCheck()
        {
            if (UserIsAuthenticated)
            {
                var rightNow = DatesUI.getDateTime();
                var usersDate = LoggedOnUser.LastActivityDate ?? DatesUI.getDateTime().AddDays(-1);
                var span = rightNow.Subtract(usersDate);
                var totalMins = span.TotalMinutes;

                if (totalMins > AppConstants.TimeSpanInMinutesToDoCheck)
                {
                    using (var unitOfWork = UnitOfWorkManager.NewUnitOfWork())
                    {
                        // Update users last activity date so we can show the latest users online
                        LoggedOnUser.LastActivityDate = DatesUI.getDateTime();

                        // Update
                        try
                        {
                            unitOfWork.Commit();
                        }
                        catch (Exception ex)
                        {
                            unitOfWork.Rollback();
                            LoggingService.Error(ex);
                        }
                    }
                }
            }

            // You can return anything to reset the timer.
            return Json(new { Timer = "reset" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetByName(string slug)
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                var member = MembershipService.GetUserBySlug(slug);
                var loggedonId = UserIsAuthenticated ? LoggedOnUser.Id : Guid.Empty;
                return View(new ViewMemberViewModel { User = member, LoggedOnUserId = loggedonId });
            }
        }

        /// <summary>
        /// Add a new user
        /// </summary>
        /// <returns></returns>
        public ActionResult Register()
        {
            if (SettingsService.GetSettings().SuspendRegistration != true)
            {
                using (UnitOfWorkManager.NewUnitOfWork())
                {
                    var user = MembershipService.CreateEmptyUser();

                    // Populate empty viewmodel
                    var viewModel = new MemberAddViewModel
                    {
                        UserName = user.UserName,
                        Email = user.Email,
                        Password = user.Password,
                        IsApproved = user.IsApproved,
                        Comment = user.Comment,
                        AllRoles = RoleService.AllRoles()
                    };

                    // See if a return url is present or not and add it
                    var returnUrl = Request["ReturnUrl"];
                    if (!string.IsNullOrEmpty(returnUrl))
                    {
                        viewModel.ReturnUrl = returnUrl;
                    }
                    return View(viewModel);
                }
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Add a new user
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Register(MemberAddViewModel userModel)
        {
            if (SettingsService.GetSettings().SuspendRegistration != true)
            {
                using (var unitOfWork = UnitOfWorkManager.NewUnitOfWork())
                {
                    // First see if there is a spam question and if so, the answer matches
                    if (!string.IsNullOrEmpty(SettingsService.GetSettings().SpamQuestion))
                    {
                        // There is a spam question, if answer is wrong return with error
                        if (userModel.SpamAnswer == null || userModel.SpamAnswer.Trim() != SettingsService.GetSettings().SpamAnswer)
                        {
                            // POTENTIAL SPAMMER!
                            ModelState.AddModelError(string.Empty, LocalizationService.GetResourceString("Error.WrongAnswerRegistration"));
                            return View();
                        }
                    }

                    // Secondly see if the email is banned
                    //if (_bannedEmailService.EmailIsBanned(userModel.Email))
                    //{
                    //    ModelState.AddModelError(string.Empty, LocalizationService.GetResourceString("Error.EmailIsBanned"));
                    //    return View();
                    //}

                    var userToSave = new MembershipUser
                    {
                        UserName = userModel.UserName, //_bannedWordService.SanitiseBannedWords(userModel.UserName),
                        Email = userModel.Email,
                        Password = userModel.Password,
                        IsApproved = userModel.IsApproved,
                        Comment = userModel.Comment,
                    };

                    var homeRedirect = false;

                    // Now check settings, see if users need to be manually authorised
                    var manuallyAuthoriseMembers = SettingsService.GetSettings().ManuallyAuthoriseNewMembers;
                    if (manuallyAuthoriseMembers)
                    {
                        userToSave.IsApproved = false;
                    }

                    var createStatus = MembershipService.CreateUser(userToSave);
                    if (createStatus != MembershipCreateStatus.Success)
                    {
                        ModelState.AddModelError(string.Empty, MembershipService.ErrorCodeToString(createStatus));
                    }
                    else
                    {
                        if (!manuallyAuthoriseMembers)
                        {
                            // If not manually authorise then log the user in
                            FormsAuthentication.SetAuthCookie(userToSave.UserName, false);
                            TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                            {
                                Message = LocalizationService.GetResourceString("Members.NowRegistered"),
                                MessageType = GenericMessages.success
                            };
                            homeRedirect = true;
                        }
                        else
                        {
                            ViewBag.Message = new GenericMessageViewModel
                            {
                                Message = LocalizationService.GetResourceString("Members.NowRegisteredNeedApproval"),
                                MessageType = GenericMessages.success
                            };
                        }

                        try
                        {
                            unitOfWork.Commit();
                            if (homeRedirect)
                            {
                                if (Url.IsLocalUrl(userModel.ReturnUrl) && userModel.ReturnUrl.Length > 1 && userModel.ReturnUrl.StartsWith("/")
                                && !userModel.ReturnUrl.StartsWith("//") && !userModel.ReturnUrl.StartsWith("/\\"))
                                {
                                    return Redirect(userModel.ReturnUrl);
                                }
                                return RedirectToAction("Index", "Home", new { area = string.Empty });
                            }
                        }
                        catch (Exception ex)
                        {
                            unitOfWork.Rollback();
                            LoggingService.Error(ex);
                            FormsAuthentication.SignOut();
                            ModelState.AddModelError(string.Empty, LocalizationService.GetResourceString("Errors.GenericMessage"));
                        }
                    }
                    return View();
                }
            }
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Log on
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOn()
        {
            // Create the empty view model
            var viewModel = new LogOnViewModel();

            // See if a return url is present or not and add it
            var returnUrl = Request["ReturnUrl"];
            if (!string.IsNullOrEmpty(returnUrl))
            {
                viewModel.ReturnUrl = returnUrl;
            }

            return View(viewModel);
        }

        /// <summary>
        /// Log on post
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult LogOn(LogOnViewModel model)
        {
            using (var unitOfWork = UnitOfWorkManager.NewUnitOfWork())
            {
                var username = model.UserName;
                var password = model.Password;

                try
                {
                    if (ModelState.IsValid)
                    {
                        var user = MembershipService.GetUser(username);
                        if (user == null)
                        {
                            ModelState.AddModelError(string.Empty, LocalizationService.GetResourceString("Username or Password is Incorrect."));
                            return View(model);
                        }
                        if (MembershipService.ValidateUser(username, password, int.Parse(ConfigUtils.GetAppSetting("MaxInvalidPasswordAttempts"))))
                        {
                            // Set last login date
                            if (user.IsApproved && !user.IsLockedOut)
                            {
                                FormsAuthentication.SetAuthCookie(username, model.RememberMe);
                                user.LastLoginDate = DatesUI.getDateTime();

                                // We use temp data because we are doing a redirect
                                //TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                                //{
                                //    Message = LocalizationService.GetResourceString("Members.NowLoggedIn"),
                                //    MessageType = GenericMessages.success
                                //};

                                if (Url.IsLocalUrl(model.ReturnUrl) && model.ReturnUrl.Length > 1 && model.ReturnUrl.StartsWith("/")
                                    && !model.ReturnUrl.StartsWith("//") && !model.ReturnUrl.StartsWith("/\\"))
                                {
                                    return Redirect(model.ReturnUrl);
                                }
                                return RedirectToAction("Index", "Home", new { area = string.Empty });
                            }
                        }

                        // Login failed
                        var loginStatus = MembershipService.LastLoginStatus;

                        switch (loginStatus)
                        {
                            case LoginAttemptStatus.UserNotFound:
                            case LoginAttemptStatus.PasswordIncorrect:
                                ModelState.AddModelError(string.Empty, LocalizationService.GetResourceString("Username or Password is Incorrect."));
                                break;

                            case LoginAttemptStatus.PasswordAttemptsExceeded:
                                ModelState.AddModelError(string.Empty, LocalizationService.GetResourceString("Password Attempts Exceeded"));
                                break;

                            case LoginAttemptStatus.UserLockedOut:
                                ModelState.AddModelError(string.Empty, LocalizationService.GetResourceString("User is Locked Out"));
                                break;

                            case LoginAttemptStatus.UserNotApproved:
                                ModelState.AddModelError(string.Empty, LocalizationService.GetResourceString("User is Not Approved"));
                                break;

                            default:
                                ModelState.AddModelError(string.Empty, LocalizationService.GetResourceString("Username or Password is Incorrect..."));
                                break;
                        }
                    }
                }

                finally
                {
                    try
                    {
                        unitOfWork.Commit();
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.Rollback();
                        LoggingService.Error(ex);
                    }
                }
                return View(model);
            }
        }

        /// <summary>
        /// Get: log off user
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOff()
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                FormsAuthentication.SignOut();
                Session.Abandon();
                ViewBag.Message = new GenericMessageViewModel
                {
                    Message = LocalizationService.GetResourceString("Members.NowLoggedOut"),
                    MessageType = GenericMessages.success
                };
                return RedirectToAction("logon", "Members", new { area = string.Empty });
            }
        }

        [Authorize]
        public string AutoComplete(string term)
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                if (!string.IsNullOrEmpty(term))
                {
                    var members = MembershipService.SearchMembers(term, 12);
                    var sb = new StringBuilder();
                    sb.Append("[").Append(Environment.NewLine);
                    for (var i = 0; i < members.Count; i++)
                    {
                        sb.AppendFormat("\"{0}\"", members[i].UserName);
                        if (i < members.Count - 1)
                        {
                            sb.Append(",");
                        }
                        sb.Append(Environment.NewLine);
                    }
                    sb.Append("]");
                    return sb.ToString();
                }
                return null;
            }
        }

        [Authorize]
        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [Authorize]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(UserChangePasswordViewModel model)
        {
            var changePasswordSucceeded = true;
            using (var unitOfWork = UnitOfWorkManager.NewUnitOfWork())
            {
                if (ModelState.IsValid)
                {
                    // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                    changePasswordSucceeded = MembershipService.ChangePassword(LoggedOnUser, model.OldPassword, model.NewPassword);

                    try
                    {
                        unitOfWork.Commit();
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.Rollback();
                        LoggingService.Error(ex);
                        changePasswordSucceeded = false;
                    }
                }
            }

            // Committed successfully carry on
            if (changePasswordSucceeded)
            {
                // We use temp data because we are doing a redirect
                TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                {
                    Message = string.Format(AppConstants.RecordStatusChanged, "Password", "Changed"),
                    MessageType = GenericMessages.success
                };
                return Redirect("~/");
            }

            TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
            {
                Message = string.Format("Password is not changed."),
                MessageType = GenericMessages.success
            };
            ModelState.AddModelError("", string.Format(AppConstants.RecordStatusChanged, "Password", "Not Changed"));
            return View(model);

        }

        public ActionResult ForgotPassword()
        {
            return View(new ForgotPasswordViewModel { UserName = "", EmailAddress = "" });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgotPassword(ForgotPasswordViewModel forgotPasswordViewModel)
        {
            var changePasswordSucceeded = true;
            var currentUser = new MembershipUser();
            var newPassword = StringUtils.RandomString(8);

            using (var unitOfWork = UnitOfWorkManager.NewUnitOfWork())
            {
                if (StringUtils.IsValidEmail(forgotPasswordViewModel.EmailAddress))
                {
                    currentUser = MembershipService.GetUserByEmail(forgotPasswordViewModel.EmailAddress);
                    if (currentUser == null)
                    {
                        ModelState.AddModelError("", LocalizationService.GetResourceString("Email not valid."));
                        return View(forgotPasswordViewModel);
                    }
                    changePasswordSucceeded = MembershipService.ResetPassword(currentUser, newPassword);

                    try
                    {
                        unitOfWork.Commit();
                    }
                    catch (Exception ex)
                    {
                        unitOfWork.Rollback();
                        LoggingService.Error(ex);
                        changePasswordSucceeded = false;
                        ModelState.AddModelError("", LocalizationService.GetResourceString("Password is failed to sent."));
                        return View(forgotPasswordViewModel);
                    }
                }
                else
                {
                    ModelState.AddModelError("", LocalizationService.GetResourceString("Email not valid."));
                    return View(forgotPasswordViewModel);
                }
            }

            // Success send new password to the user telling them password has been changed
            if (changePasswordSucceeded)
            {
                new EmailHelper().SendMailOnForgotPassword(currentUser, newPassword);
                // We use temp data because we are doing a redirect
                TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                {
                    Message = LocalizationService.GetResourceString("Password is sent on your mail."),
                    MessageType = GenericMessages.success
                };
                return RedirectToAction("LogOn");
            }

            ModelState.AddModelError("", LocalizationService.GetResourceString("Password is failed to sent."));
            return View(forgotPasswordViewModel);
        }

        [HttpPost]
        [Authorize(Roles = AppConstants.RoleNameAdministrator)]
        public ActionResult ResetUserPassword(Guid Id)
        {
            var changePasswordSucceeded = true;
            var newPassword = "password@123";
            var user = MembershipService.GetUser(Id);
            using (var unitOfWork = UnitOfWorkManager.NewUnitOfWork())
            {
                if (user == null)
                {
                    return Json("Cannot change password - user does not exist");
                }
                //if (user.Roles.Any(x => x.RoleName == AppConstants.RoleNameEmployee))
                //{
                //    newPassword = (user.employeeList.FirstOrDefault().GaddID).Replace(" ","");
                //}
                //else if (user.Roles.Any(x => x.RoleName == AppConstants.RoleNameDistributor))
                //{
                //    newPassword = user.distributorList.FirstOrDefault().DistributorID.Replace(" ", ""); 
                //}
                //else if (user.Roles.Any(x => x.RoleName == AppConstants.RoleNameVendor))
                //{
                //    newPassword = user.vendorsList.FirstOrDefault().VendorsID.Replace(" ", ""); 
                //}
                // ChangePassword will throw an exception rather than return false in certain failure scenarios.
                changePasswordSucceeded = MembershipService.ResetPassword(user, newPassword);

                try
                {
                    unitOfWork.Commit();
                }
                catch (Exception ex)
                {
                    unitOfWork.Rollback();
                    LoggingService.Error(ex);
                    changePasswordSucceeded = false;
                }

            }
            // Committed successfully carry on
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                if (changePasswordSucceeded)
                {
                    new EmailHelper().SendMailOnForgotPassword(user, newPassword);
                    return Json(LocalizationService.GetResourceString("Password is reset successfully."));
                }

                return Json(LocalizationService.GetResourceString("Password is reset failed."));
            }

        }

        public ActionResult MyProfile()
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                return View(LoggedOnUser);

            }
        }
    }
}
