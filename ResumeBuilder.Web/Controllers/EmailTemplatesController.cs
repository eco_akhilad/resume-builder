﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces.UnitOfWork;
using ResumeBuilder.Domain.Constants;
using ResumeBuilder.ViewModels;
using ResumeBuilder.Web.Application;
using ResumeBuilder.Utilities;
using System.Net;
using System.Text;

namespace ResumeBuilder.Web.Controllers
{
    public class EmailTemplatesController : BaseController
    {
        private readonly IEmailTemplatesService _emailtemplatesService;

        public EmailTemplatesController(ILoggingService loggingService,
            IUnitOfWorkManager unitOfWorkManager,
            IMembershipService membershipService,
            IRoleService roleService,
            ISettingsService settingsService,
            IEmailTemplatesService emailtemplatesService)
            : base(loggingService, unitOfWorkManager, membershipService, roleService, settingsService)
        {
            _emailtemplatesService = emailtemplatesService;
        }

        [Authorize]
        public ActionResult Index()
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                var emailtemplates = _emailtemplatesService.AllEmailTemplates();
                return View(new EmailTemplatesViewModel
                {
                    emailtemplates = emailtemplates
                });
            }
        }

        [Authorize]
        public ActionResult Add()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Add(EmailTemplatesAddModel emailtemplatesAdd)
        {
            using (var newUnitOfWork = UnitOfWorkManager.NewUnitOfWork())
            {
                EmailTemplates emailtemplates = new EmailTemplates();
                int IEmailTemplatesCount = _emailtemplatesService.GetCount();

                emailtemplates.EmailTemplateId = AppHelpers.GetNewId("ET", IEmailTemplatesCount + 1);
                emailtemplates.EmailTemplateName = emailtemplatesAdd.TemplateName;
                emailtemplates.EmailTemplateSubject = emailtemplatesAdd.TemplateSubject;
                emailtemplates.EmailTemplateBody = emailtemplatesAdd.TemplateBody;
                emailtemplates.LastModified = System.DateTime.Now;
                emailtemplates.IsActive = emailtemplatesAdd.IsActive;

                _emailtemplatesService.Create(emailtemplates);

                try
                {
                    newUnitOfWork.Commit();
                    //Use temp data as itts redirect(when redirection occure it uses this data)
                    TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                    {
                        Message = "Your Email Template is Added Successfully",
                        MessageType = GenericMessages.success,
                    };
                }
                catch (Exception ex)
                {
                    newUnitOfWork.Rollback();
                    LoggingService.Error(ex);
                    //Use temp data as itts redirect(when redirection occure it uses this data)
                    TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                    {
                        Message = string.Format(AppHelpers.GetExceptionMessage(ex)),
                        MessageType = GenericMessages.error,
                    };
                    //throw new Exception("Error while adding Email Template");
                }
                return RedirectToAction("Index");
            }
        }

        [Authorize]
        public ActionResult Details(Guid Id)
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                EmailTemplates emailtemplates = _emailtemplatesService.GetEmailById(Id);
                return View(new EmailTemplatesDetails
                {
                    emailtemplates = emailtemplates
                });
            }
        }

        [Authorize]
        public ActionResult Edit(Guid Id)
        {
            using (UnitOfWorkManager.NewUnitOfWork())
            {
                EmailTemplates emailtemplates = _emailtemplatesService.GetEmailById(Id);
                return View(new EmailTemplatesEditModel
                {
                    Id = emailtemplates.Id,
                    TemplateId = emailtemplates.EmailTemplateId,
                    TemplateName = emailtemplates.EmailTemplateName,
                    TemplateSubject = emailtemplates.EmailTemplateSubject,
                    TemplateBody = emailtemplates.EmailTemplateBody,
                    LastModified = System.DateTime.Now,
                    IsActive = emailtemplates.IsActive,
                });
            }
        }

        [Authorize]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(EmailTemplatesEditModel emailtemplatesEdit)
        {
            using (var newUnitOfWork = UnitOfWorkManager.NewUnitOfWork())
            {
                EmailTemplates emailtemplates = _emailtemplatesService.GetEmailById(emailtemplatesEdit.Id);
                if (emailtemplates != null)
                {
                    emailtemplates.EmailTemplateName = emailtemplatesEdit.TemplateName;
                    emailtemplates.EmailTemplateSubject = emailtemplatesEdit.TemplateSubject;
                    emailtemplates.EmailTemplateBody = emailtemplatesEdit.TemplateBody;
                    emailtemplates.LastModified = System.DateTime.Now;
                    //emailtemplates.IsActive = emailtemplatesEdit.IsActive;

                    try
                    {
                        newUnitOfWork.Commit();
                        TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                        {
                            Message = "Successfully edited Email Template",
                            MessageType = GenericMessages.success
                        };
                    }
                    catch (Exception ex)
                    {
                        newUnitOfWork.Rollback();
                        LoggingService.Error(ex);
                        TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                        {
                            Message = string.Format(AppHelpers.GetExceptionMessage(ex)),
                            MessageType = GenericMessages.error
                        };
                        //throw new Exception("Error Editing Email Template");
                    }
                }
                else
                {
                    LoggingService.Error("Error editing Email Template ID:" + emailtemplates.Id);
                    // Use temp data as its a redirect
                    TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                    {
                        Message = "Error editing Email Template",
                        MessageType = GenericMessages.error
                    };
                }
                return RedirectToAction("Index");
            }
        }

        [Authorize]
        public ActionResult Delete(FormCollection fm)
        {
            using (var NewUnitWork = UnitOfWorkManager.NewUnitOfWork())
            {
                bool fmVal = Convert.ToBoolean(Request.Form["chkSubmit"]);
                foreach (string sName in fm.AllKeys)
                {
                    try
                    {
                        EmailTemplates delemailtemplates = _emailtemplatesService.GetEmailById(AppHelpers.StringToGuid(sName));
                        if (delemailtemplates != null)
                        {
                            delemailtemplates.IsActive = fmVal;
                            delemailtemplates.LastModified = DateUtils.getCurrentDateTime();
                        }
                    }
                    catch
                    {
                        continue;
                    }
                }

                try
                {
                    NewUnitWork.Commit();
                    // Use temp data as its a redirect
                    TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                    {
                        Message = "Email Template status changed...",
                        MessageType = GenericMessages.success
                    };
                }
                catch (Exception ex)
                {
                    NewUnitWork.Rollback();
                    LoggingService.Error(ex);
                    TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                    {
                        Message = string.Format(AppHelpers.GetExceptionMessage(ex)),
                        MessageType = GenericMessages.error
                    };
                }
                return RedirectToAction("Index");
            }
        }
        [Authorize(Roles = AppConstants.RoleNameAdministrator)]
        public ActionResult SetStatus(FormCollection frm)
        {
            using (var NewUnitOfWork = UnitOfWorkManager.NewUnitOfWork())
            {
                try
                {
                    bool statusVal = Convert.ToBoolean(Request.Form["chkSubmit"].Replace(",", ""));
                    foreach (string id in frm.AllKeys)
                    {
                        try
                        {
                            EmailTemplates changeStatus = _emailtemplatesService.GetEmailById(AppHelpers.StringToGuid(id));
                            if (changeStatus != null)
                            {
                                changeStatus.IsActive = statusVal;
                                changeStatus.LastModified = DateUtils.getCurrentDateTime();
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                    NewUnitOfWork.Commit();
                    // Use temp data as its a redirect
                    TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                    {
                        Message = string.Format(AppConstants.RecordStatusChanged, "Email Template", statusVal ? " Activated" : "Deactivated"),
                        MessageType = GenericMessages.success,
                    };
                }
                catch (Exception ex)
                {
                    NewUnitOfWork.Rollback();
                    LoggingService.Error(ex);
                    TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                    {
                        Message = string.Format(AppConstants.RecordSaveException + AppHelpers.GetExceptionMessage(ex)),
                        MessageType = GenericMessages.error

                    };
                }
                return RedirectToAction("Index");
            }
        }
    }
}
