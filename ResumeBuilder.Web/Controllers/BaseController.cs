﻿using System.Linq;
using System.Web.Mvc;
using System.Threading;
using System.Web.Routing;
using System.Globalization;
using ResumeBuilder.ViewModels;
using ResumeBuilder.Domain.Constants;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Domain.Interfaces.UnitOfWork;

namespace ResumeBuilder.Web.Controllers
{
    /// <summary>
    /// A base class for the white site controllers
    /// </summary>
    public class BaseController : Controller
    {
        protected readonly IUnitOfWorkManager UnitOfWorkManager;
        protected readonly IMembershipService MembershipService;
        protected readonly ISettingsService SettingsService;
        protected readonly ILoggingService LoggingService;
        protected readonly IHistoryService HistoryService;
        protected readonly IRoleService RoleService;

        public readonly MembershipUser LoggedOnUser;
        public readonly MembershipRole UsersRole;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="loggingService"> </param>
        /// <param name="unitOfWorkManager"> </param>
        /// <param name="membershipService"></param>
        /// <param name="localizationService"> </param>
        /// <param name="roleService"> </param>
        /// <param name="settingsService"> </param>
        /// <param name="HistoryService" optinal> </param>
        public BaseController(ILoggingService loggingService, IUnitOfWorkManager unitOfWorkManager, IMembershipService membershipService,
                            IRoleService roleService, ISettingsService settingsService)
        {

            UnitOfWorkManager = unitOfWorkManager;
            MembershipService = membershipService;
            RoleService = roleService;
            SettingsService = settingsService;
            LoggingService = loggingService;

            LoggedOnUser = UserIsAuthenticated ? MembershipService.GetUser(UserName) : null;
            UsersRole = LoggedOnUser == null ? null : LoggedOnUser.Roles.FirstOrDefault();

        }
        public BaseController(ILoggingService loggingService, IUnitOfWorkManager unitOfWorkManager, IMembershipService membershipService,
                            IRoleService roleService, ISettingsService settingsService, IHistoryService historyService)
        {
            UnitOfWorkManager = unitOfWorkManager;
            MembershipService = membershipService;
            RoleService = roleService;
            SettingsService = settingsService;
            LoggingService = loggingService;
            HistoryService = historyService;

            LoggedOnUser = UserIsAuthenticated ? MembershipService.GetUser(UserName) : null;
            UsersRole = LoggedOnUser == null ? null : LoggedOnUser.Roles.FirstOrDefault();

        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var controller = filterContext.RouteData.Values;
            if (SettingsService.GetSettings().IsClosed && !System.Web.Security.Roles.IsUserInRole(AppConstants.RoleNameSuperAdmin))
            {
                if (controller["controller"].ToString().ToLower() != "closed" && controller["action"].ToString().ToLower() != "logon")
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary { { "controller", "Closed" }, { "action", "Index" } });
                }
            }
            if (UserIsAuthenticated)
            {
                //ViewData[AppConstants.LogInUserViewBagName] = LoggedOnUser.Slug;
            }

        }

        protected bool UserIsAuthenticated
        {
            get { return System.Web.HttpContext.Current.User.Identity.IsAuthenticated; }
        }

        protected string UserName
        {
            get { return UserIsAuthenticated ? System.Web.HttpContext.Current.User.Identity.Name : null; }
        }

        internal ActionResult ErrorToHomePage(string errorMessage)
        {
            // Use temp data as its a redirect
            TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
            {
                Message = errorMessage,
                MessageType = GenericMessages.error
            };
            // Not allowed in here so
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Gets or sets the current UI culture.
        /// </summary>
        /// <remarks>
        /// Values meaning: 0 = InvariantCulture (en-US), 2 = ro-RO, 1 = de-DE.
        /// </remarks>
        public int CurrentUICulture
        {
            get
            {
                if (Thread.CurrentThread.CurrentUICulture.Name == "de-DE")
                    return 1;
                else if (Thread.CurrentThread.CurrentUICulture.Name == "ro-RO")
                    return 2;
                else
                    return 0;
            }
            set
            {
                if (value == 1)
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("de-DE");
                else if (value == 2)
                    Thread.CurrentThread.CurrentUICulture = new CultureInfo("ro-RO");
                else
                    Thread.CurrentThread.CurrentUICulture = CultureInfo.InvariantCulture;

                // Set the thread's CurrentCulture the same as CurrentUICulture.
                Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;
            }
        }
    }

    public class UserNotLoggedOnException : System.Exception
    {

    }
}