﻿using System.Web.Mvc;

namespace ResumeBuilder.Web.Areas.Admin
{
    public class AdminAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "Admin";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Admin_edituserroute",
                "Admin/{controller}/{action}/{userId}",
                new { controller = "Admin", action = "Index", userId = UrlParameter.Optional }
            );
            context.MapRoute(
                "Admin_pagingroute",
                "Admin/{controller}/{action}/{page}",
                new { controller = "Account", action = "Index", page = UrlParameter.Optional }
            );
            context.MapRoute(
                "Admin_defaultroute",
                "Admin/{controller}/{action}/{id}",
                new { controller = "Admin", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}