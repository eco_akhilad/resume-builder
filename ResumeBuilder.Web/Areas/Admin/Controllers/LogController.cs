﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using ResumeBuilder.Domain.Constants;
using ResumeBuilder.Domain.DomainModel;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Domain.Interfaces.UnitOfWork;
using ResumeBuilder.ViewModels.Admin;
using ResumeBuilder.ViewModels;

namespace ResumeBuilder.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = AppConstants.RoleNameSuperAdmin)]
    public class LogController : BaseAdminController
    {
        public LogController(ILoggingService loggingService, IUnitOfWorkManager unitOfWorkManager, IMembershipService membershipService, ISettingsService settingsService) :
            base(loggingService, unitOfWorkManager, membershipService, settingsService)
        {

        }

        public ActionResult Index()
        {
            IList<LogEntry> logs = new List<LogEntry>();

            try
            {
                logs = LoggingService.ListLogFile();
            }
            catch (Exception ex)
            {
                var err = string.Format("Unable to access logs: {0}", ex.Message);
                TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
                {
                    Message = err,
                    MessageType = GenericMessages.error
                };

                LoggingService.Error(err);
            }

            return View(new ListLogViewModel { LogFiles = logs });
        }

        public ActionResult ClearLog()
        {
            LoggingService.ClearLogFiles();

            TempData[AppConstants.MessageViewBagName] = new GenericMessageViewModel
            {
                Message = "Log File Cleared",
                MessageType = GenericMessages.success
            };
            return RedirectToAction("Index");
        }

    }
}
