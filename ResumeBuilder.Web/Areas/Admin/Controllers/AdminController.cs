﻿using System.Web.Mvc;
using ResumeBuilder.Domain.Constants;
using ResumeBuilder.Domain.Interfaces.Services;
using ResumeBuilder.Domain.Interfaces.UnitOfWork;
using System.Text;
using System.Collections.Specialized;
using System.IO;

namespace ResumeBuilder.Web.Areas.Admin.Controllers
{
    [Authorize(Roles = AppConstants.RoleNameSuperAdmin)]
    public class AdminController : BaseAdminController
    {

        public AdminController(ILoggingService loggingService, IUnitOfWorkManager unitOfWorkManager, IMembershipService membershipService, ISettingsService settingsService)
            : base(loggingService, unitOfWorkManager, membershipService, settingsService)
        {
        }

        /// <summary>
        /// Default page for the admin area
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

    }
}
