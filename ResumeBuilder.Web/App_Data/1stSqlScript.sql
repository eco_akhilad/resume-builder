/****** Object:  ForeignKey [FK_History_MembershipUser]    Script Date: 12/24/2013 16:38:15 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_History_MembershipUser]') AND parent_object_id = OBJECT_ID(N'[dbo].[History]'))
ALTER TABLE [dbo].[History] DROP CONSTRAINT [FK_History_MembershipUser]
GO
/****** Object:  ForeignKey [FK_MembershipUsersInRoles_MembershipRole]    Script Date: 12/24/2013 16:39:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MembershipUsersInRoles_MembershipRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[MembershipUsersInRoles]'))
ALTER TABLE [dbo].[MembershipUsersInRoles] DROP CONSTRAINT [FK_MembershipUsersInRoles_MembershipRole]
GO
/****** Object:  ForeignKey [FK_MembershipUsersInRoles_MembershipUser]    Script Date: 12/24/2013 16:39:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MembershipUsersInRoles_MembershipUser]') AND parent_object_id = OBJECT_ID(N'[dbo].[MembershipUsersInRoles]'))
ALTER TABLE [dbo].[MembershipUsersInRoles] DROP CONSTRAINT [FK_MembershipUsersInRoles_MembershipUser]
GO
/****** Object:  ForeignKey [FK_Settings_MembershipRole]    Script Date: 12/24/2013 16:39:39 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Settings_MembershipRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[Settings]'))
ALTER TABLE [dbo].[Settings] DROP CONSTRAINT [FK_Settings_MembershipRole]
GO
/****** Object:  Table [dbo].[History]    Script Date: 12/24/2013 16:38:15 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_History_MembershipUser]') AND parent_object_id = OBJECT_ID(N'[dbo].[History]'))
ALTER TABLE [dbo].[History] DROP CONSTRAINT [FK_History_MembershipUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[History]') AND type in (N'U'))
DROP TABLE [dbo].[History]
GO
/****** Object:  Table [dbo].[MembershipUsersInRoles]    Script Date: 12/24/2013 16:39:19 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MembershipUsersInRoles_MembershipRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[MembershipUsersInRoles]'))
ALTER TABLE [dbo].[MembershipUsersInRoles] DROP CONSTRAINT [FK_MembershipUsersInRoles_MembershipRole]
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MembershipUsersInRoles_MembershipUser]') AND parent_object_id = OBJECT_ID(N'[dbo].[MembershipUsersInRoles]'))
ALTER TABLE [dbo].[MembershipUsersInRoles] DROP CONSTRAINT [FK_MembershipUsersInRoles_MembershipUser]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MembershipUsersInRoles]') AND type in (N'U'))
DROP TABLE [dbo].[MembershipUsersInRoles]
GO
/****** Object:  Table [dbo].[Settings]    Script Date: 12/24/2013 16:39:39 ******/
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Settings_MembershipRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[Settings]'))
ALTER TABLE [dbo].[Settings] DROP CONSTRAINT [FK_Settings_MembershipRole]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Settings]') AND type in (N'U'))
DROP TABLE [dbo].[Settings]
GO
/****** Object:  Table [dbo].[EmailTemplates]    Script Date: 12/24/2013 16:37:54 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmailTemplates]') AND type in (N'U'))
DROP TABLE [dbo].[EmailTemplates]
GO
/****** Object:  Table [dbo].[MembershipRole]    Script Date: 12/24/2013 16:38:37 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MembershipRole]') AND type in (N'U'))
DROP TABLE [dbo].[MembershipRole]
GO
/****** Object:  Table [dbo].[MembershipUser]    Script Date: 12/24/2013 16:38:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MembershipUser]') AND type in (N'U'))
DROP TABLE [dbo].[MembershipUser]
GO
/****** Object:  Table [dbo].[MembershipUser]    Script Date: 12/24/2013 16:38:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MembershipUser]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MembershipUser](
	[Id] [uniqueidentifier] NOT NULL,
	[UserName] [nvarchar](150) NOT NULL,
	[Password] [nvarchar](128) NOT NULL,
	[PasswordSalt] [nvarchar](128) NULL,
	[Email] [nvarchar](256) NOT NULL,
	[PasswordQuestion] [nvarchar](256) NULL,
	[PasswordAnswer] [nvarchar](256) NULL,
	[IsApproved] [bit] NOT NULL,
	[IsLockedOut] [bit] NULL,
	[IsDeleted] [bit] NULL,
	[CreateDate] [datetime] NOT NULL,
	[LastLoginDate] [datetime] NOT NULL,
	[LastPasswordChangedDate] [datetime] NOT NULL,
	[LastLockoutDate] [datetime] NOT NULL,
	[FailedPasswordAttemptCount] [int] NOT NULL,
	[FailedPasswordAnswerAttempt] [int] NOT NULL,
	[Slug] [nvarchar](150) NOT NULL,
	[Language] [int] NOT NULL,
	[Comment] [ntext] NULL,
	[Signature] [nvarchar](1000) NULL,
	[Location] [nvarchar](100) NULL,
	[Website] [nvarchar](100) NULL,
	[LoginIdExpires] [datetime] NULL,
	[MiscAccessToken] [nvarchar](250) NULL,
	[LastActivityDate] [datetime] NULL
 CONSTRAINT [PK_MembershipUser] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO
INSERT [dbo].[MembershipUser] ([Id], [UserName], [Password], [PasswordSalt], [Email], [PasswordQuestion], [PasswordAnswer], [IsApproved], [IsLockedOut], [IsDeleted], [CreateDate], [LastLoginDate], [LastPasswordChangedDate], [LastLockoutDate], [FailedPasswordAttemptCount], [FailedPasswordAnswerAttempt], [Slug], [Language], [Comment], [Signature], [Location], [Website], [LoginIdExpires], [MiscAccessToken], [LastActivityDate]) VALUES (N'85cecbaf-9d57-4726-999c-a1d6006bf3c8', N'superadmin', N'XWkC++EqGSzHVQredHh7pGHtcyaSasqBiU0hXfU0jx8=', N'XliPv9heKQhwHnT9qoxeFkOXiwX7JEv7', N'superadmin@ecotech.com', NULL, NULL, 1, 0, 0, CAST(0x0000A1D6006BF3D6 AS DateTime), CAST(0x0000A28C00B9C92E AS DateTime), CAST(0x0000A27A0034226C AS DateTime), CAST(0x0000A1E400B2B912 AS DateTime), 0, 0, N'admin', 0, NULL, NULL, NULL, NULL, NULL, NULL, CAST(0x0000A27A00342271 AS DateTime))
/****** Object:  Table [dbo].[MembershipRole]    Script Date: 12/24/2013 16:38:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MembershipRole]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MembershipRole](
	[Id] [uniqueidentifier] NOT NULL,
	[RoleName] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_MembershipRole] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[MembershipRole] ([Id], [RoleName]) VALUES (N'7f64d04e-da45-4762-8603-a1d6006bee60', N'Employee')
INSERT [dbo].[MembershipRole] ([Id], [RoleName]) VALUES (N'3c599381-2305-4e05-a8e5-a1d6006bee60', N'Users')
INSERT [dbo].[MembershipRole] ([Id], [RoleName]) VALUES (N'45b4ab95-919f-4303-a9e4-a1d6006bee60', N'SuperAdmin')
INSERT [dbo].[MembershipRole] ([Id], [RoleName]) VALUES (N'bb747571-349d-4d05-a547-a21f00db6db7', N'Administrator')
/****** Object:  Table [dbo].[EmailTemplates]    Script Date: 12/24/2013 16:37:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EmailTemplates]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[EmailTemplates](
	[Id] [uniqueidentifier] NOT NULL,
	[EmailTemplateId] [nvarchar](50) NOT NULL,
	[EmailTemplateName] [nvarchar](500) NULL,
	[EmailTemplateSubject] [nvarchar](500) NULL,
	[EmailTemplateBody] [nvarchar](max) NULL,
	[LastModified] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_EmailTemplates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[EmailTemplates] ([Id], [EmailTemplateId], [EmailTemplateName], [EmailTemplateSubject], [EmailTemplateBody], [LastModified], [IsActive]) VALUES (N'467591a0-e022-43ee-b283-a2670139f80f', N'ET0001', N'Forgot-Password', N'Mairec :: Password reset mail', N'Dear #USER#,

You have requested that your password is reset.

Your new password is below:
Username => #USERNAME#
Password => #PASSWORD#

Regards,
Mairec - Admin', CAST(0x0000A2670139F812 AS DateTime), 1)
/****** Object:  Table [dbo].[Settings]    Script Date: 12/24/2013 16:39:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Settings]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Settings](
	[Id] [uniqueidentifier] NOT NULL,
	[ApplicationName] [nvarchar](500) NULL,
	[ApplicationUrl] [nvarchar](500) NULL,
	[IsClosed] [bit] NULL,
	[EnableRSSFeeds] [bit] NULL,
	[DisplayEditedBy] [bit] NULL,
	[EnablePostFileAttachments] [bit] NULL,
	[EnableMarkAsSolution] [bit] NULL,
	[EnableSpamReporting] [bit] NULL,
	[EnableMemberReporting] [bit] NULL,
	[ManuallyAuthoriseNewMembers] [bit] NULL,
	[EmailAdminOnNewMemberSignUp] [bit] NULL,
	[EnableSignatures] [bit] NULL,
	[AdminEmailAddress] [nvarchar](100) NULL,
	[NotificationReplyEmail] [nvarchar](100) NULL,
	[SMTP] [nvarchar](100) NULL,
	[SMTPUsername] [nvarchar](100) NULL,
	[SMTPPort] [nvarchar](10) NULL,
	[SMTPEnableSSL] [bit] NULL,
	[SMTPPassword] [nvarchar](100) NULL,
	[NewMemberStartingRole] [uniqueidentifier] NULL,
	[EnableAkisment] [bit] NULL,
	[AkismentKey] [nvarchar](100) NULL,
	[CurrentDatabaseVersion] [nvarchar](10) NULL,
	[SpamQuestion] [nvarchar](500) NULL,
	[SpamAnswer] [nvarchar](500) NULL,
	[SuspendRegistration] [bit] NULL,
 CONSTRAINT [PK_Settings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
INSERT [dbo].[Settings] ([Id], [ApplicationName], [ApplicationUrl], [IsClosed], [EnableRSSFeeds], [DisplayEditedBy], [EnablePostFileAttachments], [EnableMarkAsSolution], [EnableSpamReporting], [EnableMemberReporting], [ManuallyAuthoriseNewMembers], [EmailAdminOnNewMemberSignUp], [EnableSignatures], [AdminEmailAddress], [NotificationReplyEmail], [SMTP], [SMTPUsername], [SMTPPort], [SMTPEnableSSL], [SMTPPassword], [NewMemberStartingRole], [EnableAkisment], [AkismentKey], [CurrentDatabaseVersion], [SpamQuestion], [SpamAnswer], [SuspendRegistration]) VALUES (N'fb39f029-3f5f-4e61-a349-a1d6006bf345', N':: SKF ::', N'http://www.skf.com/', 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, N'ecotech188@gmail.com', N'noreply@myemail.com', N'smtp.gmail.com', N'ecotech188@gmail.com', N'587', 1, N'paritosh2', N'3c599381-2305-4e05-a8e5-a1d6006bee60', 0, NULL, NULL, NULL, NULL, 0)
/****** Object:  Table [dbo].[MembershipUsersInRoles]    Script Date: 12/24/2013 16:39:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[MembershipUsersInRoles]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[MembershipUsersInRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleIdentifier] [uniqueidentifier] NOT NULL,
	[UserIdentifier] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_MembershipUsersInRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
END
GO
SET IDENTITY_INSERT [dbo].[MembershipUsersInRoles] ON
INSERT [dbo].[MembershipUsersInRoles] ([Id], [RoleIdentifier], [UserIdentifier]) VALUES (1, N'45b4ab95-919f-4303-a9e4-a1d6006bee60', N'85cecbaf-9d57-4726-999c-a1d6006bf3c8')
SET IDENTITY_INSERT [dbo].[MembershipUsersInRoles] OFF
/****** Object:  Table [dbo].[History]    Script Date: 12/24/2013 16:38:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[History]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[History](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Timestamp] [datetime] NOT NULL,
	[Data] [ntext] NOT NULL,
	[TableName] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NOT NULL,
	[User_Id] [uniqueidentifier] NOT NULL,
	[Item_Id] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_History] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
END
GO

/****** Object:  ForeignKey [FK_History_MembershipUser]    Script Date: 12/24/2013 16:38:15 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_History_MembershipUser]') AND parent_object_id = OBJECT_ID(N'[dbo].[History]'))
ALTER TABLE [dbo].[History]  WITH CHECK ADD  CONSTRAINT [FK_History_MembershipUser] FOREIGN KEY([User_Id])
REFERENCES [dbo].[MembershipUser] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_History_MembershipUser]') AND parent_object_id = OBJECT_ID(N'[dbo].[History]'))
ALTER TABLE [dbo].[History] CHECK CONSTRAINT [FK_History_MembershipUser]
GO
/****** Object:  ForeignKey [FK_MembershipUsersInRoles_MembershipRole]    Script Date: 12/24/2013 16:39:19 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MembershipUsersInRoles_MembershipRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[MembershipUsersInRoles]'))
ALTER TABLE [dbo].[MembershipUsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_MembershipUsersInRoles_MembershipRole] FOREIGN KEY([RoleIdentifier])
REFERENCES [dbo].[MembershipRole] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MembershipUsersInRoles_MembershipRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[MembershipUsersInRoles]'))
ALTER TABLE [dbo].[MembershipUsersInRoles] CHECK CONSTRAINT [FK_MembershipUsersInRoles_MembershipRole]
GO
/****** Object:  ForeignKey [FK_MembershipUsersInRoles_MembershipUser]    Script Date: 12/24/2013 16:39:19 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MembershipUsersInRoles_MembershipUser]') AND parent_object_id = OBJECT_ID(N'[dbo].[MembershipUsersInRoles]'))
ALTER TABLE [dbo].[MembershipUsersInRoles]  WITH CHECK ADD  CONSTRAINT [FK_MembershipUsersInRoles_MembershipUser] FOREIGN KEY([UserIdentifier])
REFERENCES [dbo].[MembershipUser] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_MembershipUsersInRoles_MembershipUser]') AND parent_object_id = OBJECT_ID(N'[dbo].[MembershipUsersInRoles]'))
ALTER TABLE [dbo].[MembershipUsersInRoles] CHECK CONSTRAINT [FK_MembershipUsersInRoles_MembershipUser]
GO
/****** Object:  ForeignKey [FK_Settings_MembershipRole]    Script Date: 12/24/2013 16:39:39 ******/
IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Settings_MembershipRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[Settings]'))
ALTER TABLE [dbo].[Settings]  WITH CHECK ADD  CONSTRAINT [FK_Settings_MembershipRole] FOREIGN KEY([NewMemberStartingRole])
REFERENCES [dbo].[MembershipRole] ([Id])
GO
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Settings_MembershipRole]') AND parent_object_id = OBJECT_ID(N'[dbo].[Settings]'))
ALTER TABLE [dbo].[Settings] CHECK CONSTRAINT [FK_Settings_MembershipRole]
GO
